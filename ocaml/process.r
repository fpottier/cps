#!/usr/bin/env Rscript

require(ggplot2)

pwd <- "/home/fpottier/dev/cps/ocaml"
data <- read.table(paste(pwd, "data", sep="/"),header=TRUE)
data <- transform(data, constant = nanos/25/size/log(size))

# Draw a graph with both lines and points.
# X axis: size.
# Y axis: constant, that is, (nanoseconds per run) divided by (size . log size).
# This line should be horizontal.
p <- qplot(data$size, data$constant) +
     geom_point() +
     scale_x_log10() +
     xlab("n (tree size)") +
     ylab("time / nlog n")

print(p)

# Linear regression.
model <- lm(constant ~ size, data)
summary(model)
