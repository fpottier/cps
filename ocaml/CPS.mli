val add : int -> int -> int

val sub : int -> int -> int

type var = int

val funcomp : ('a1 -> 'a2) -> ('a2 -> 'a3) -> 'a1 -> 'a3

val scons : 'a1 -> (var -> 'a1) -> var -> 'a1

val lift : var -> var -> var

type ('t1, 't2) _bind = 't2

type 'term ids = var -> 'term

val ids0 : 'a1 ids -> var -> 'a1

type 'term rename = (var -> var) -> 'term -> 'term

val rename0 : 'a1 rename -> (var -> var) -> 'a1 -> 'a1

type 'term subst = (var -> 'term) -> 'term -> 'term

val subst0 : 'a1 subst -> (var -> 'a1) -> 'a1 -> 'a1

val ren : 'a1 ids -> (var -> var) -> var -> 'a1

val up : 'a1 ids -> 'a1 rename -> (var -> 'a1) -> var -> 'a1

val upren : (var -> var) -> var -> var

type term =
| Var of var
| Lam of (term, term) _bind
| App of term * term
| Let of term * (term, term) _bind

val ids_term : term ids

val rename_term : term rename

val subst_term : term subst

type continuation =
| O of term
| M of term

val apply : continuation -> term -> term

val reify : continuation -> term

val substc : (var -> term) -> continuation -> continuation

val cps : term -> continuation -> term

val init : continuation

val cpsinit : term -> term

type map = int IntMap.t

val empty : unit -> map

val add0 : int -> int -> map -> map

val find : int -> map -> int

type renaming = (int * map) * int

val _id : unit -> renaming

val _up : renaming -> renaming

val interpret : renaming -> var -> var

type rterm = int -> term

val var0 : int -> int -> rterm

val lambda : (rterm -> rterm) -> rterm

val app : rterm -> rterm -> rterm

val svar : var -> (int * map) -> rterm

val sbind : ((int * map) -> int -> term) -> (int * map) -> rterm

val slambda : (int * map) -> ((int * map) -> int -> term) -> rterm

val slet :
  (int * map) -> (int -> term) -> ((int * map) -> int -> term) -> rterm

type rcontinuation =
| RO of rterm
| RM of (rterm -> rterm)

val rapply : rcontinuation -> rterm -> rterm

val rreify : rcontinuation -> rterm

val rcps : (int * map) -> term -> rcontinuation -> rterm

val rinit : rcontinuation

val rcpsinit : term -> term
