open CPS

(* -------------------------------------------------------------------------- *)

(* Enumerate all variables between 0 and [n] excluded. *)

let var (n : int) (k : term -> unit) : unit =
  for x = 0 to n - 1 do
    k (Var x)
  done

(* Enumerate all manners of splitting an integer [s]. *)

let split (s : int) (k : int -> int -> unit) : unit =
  for i = 0 to s do
    k i (s - i)
  done

(* Enumerate all terms of size exactly [s] with at most [n] variables. *)

let generate_let =
  true

let rec term (s : int) (n : int) (k : term -> unit) : unit =
  if s = 0 then
    (* We must use a variable. *)
    var n k
  else begin
    (* We can use a lambda. *)
    term (s-1) (n+1) (fun t -> k (Lam t));
    (* We can use an application or a [let] construct. *)
    split (s-1) (fun s1 s2 ->
      term s1 n (fun t1 ->
        term s2 n (fun t2 ->
          k (App (t1, t2));
        );
        if generate_let then
          term s2 (n+1) (fun t2 ->
            k (Let (t1, t2));
          )
      )
    )
  end

(* -------------------------------------------------------------------------- *)

(* Pick a variable between 0 and [n] excluded. *)

let var n =
  Random.int n

(* Pick a splitting of an integer [s]. *)

let split_app s =
  let i = Random.int (1 + s) in
  i, s - i

let skewed =
  true

let split_let s =
  if skewed then
    (* Skew [let] constructs towards the right. *)
    let i = min 20 (Random.int (1 + s) / 4) in
    i, s - i
  else
    split_app s

(* Pick a term of size exactly [s] with at most [n] variables. *)

let rec random s n =
  if s = 0 then
    (* We must use a variable. *)
    Var (var n)
  else
    (* At the root of the term, we always place a lambda, so we
       have at least one variable and we will never call [var 0],
       which would fail. *)
    match (if n = 0 then 0 else Random.int 3) with
    | 0 ->
        Lam (random (s-1) (n+1))
    | 1 ->
        let s1, s2 = split_app (s-1) in
        let t1 = random s1 n
        and t2 = random s2 n in
        App (t1, t2)
    | 2 ->
        let s1, s2 = split_let (s-1) in
        let t1 = random s1 n
        and t2 = random s2 (n+1) in
        Let (t1, t2)
    | _ ->
        assert false

(* -------------------------------------------------------------------------- *)

(* Stepping in call-by-value. *)

let is_value = function
  | Var _
  | Lam _ ->
      true
  | App _
  | Let _ ->
     false

(* We are working with closed terms, so irreducible terms must be values. *)

let rec step t =
  match t with
  | Var _
  | Lam _ ->
      None
  | App (t1, t2) ->
      begin match step t1 with
      | Some t1 ->
          Some (App (t1, t2))
      | None ->
          assert (is_value t1);
          match step t2 with
          | Some t2 ->
              Some (App (t1, t2))
          | None ->
              match t1 with
              | Lam t1 ->
                  assert (is_value t2);
                  let sigma x =
                    if x = 0 then t2 else Var (x - 1)
                  in
                  Some (subst_term sigma t1)
              | _ ->
                  assert false
      end
  | Let (t1, t2) ->
      match step t1 with
      | Some t1 ->
          Some (Let (t1, t2))
      | None ->
          assert (is_value t1);
          let sigma x =
            if x = 0 then t1 else Var (x - 1)
          in
          Some (subst_term sigma t2)

(* Testing whether [t1] reduces (in several steps) to [t2]. *)

let rec reduces (t1 : term) (t2 : term) : bool =
  if t1 = t2 then
    true
  else
    match step t1 with
    | None ->
        false
    | Some t1 ->
        reduces t1 t2

(* -------------------------------------------------------------------------- *)

(* A term printer. *)

open Printf

let rec print f = function
  | Var x ->
      fprintf f "(Var %d)" x
  | Lam t ->
      fprintf f "(Lam %a)" print t
  | App (t1, t2) ->
      fprintf f "(App %a %a)" print t1 print t2
  | Let (t1, t2) ->
      fprintf f "(Let %a %a)" print t1 print t2

let print t =
  fprintf stdout "%a\n" print t

(* -------------------------------------------------------------------------- *)
