open Printf
open CPS
open Common

(* -------------------------------------------------------------------------- *)

(* Printing a term and its CPS translation. *)

let print_translation t u =
  print t;
  printf "(CPS translation:)\n";
  print u

(* -------------------------------------------------------------------------- *)

(* This little program searches for a counter-example to the simple simulation
   diagram, which states that if [cbv t1 t2] holds, then [cps t1 init] must
   reduce (in zero, one or more steps of [cbv]) to [cps t2 icd]. This simple
   diagram is true in the pure lambda-calculus, but is violated once [Let]
   constructs are introduced. *)

let () =
  if Array.length Sys.argv < 2 then begin
    printf "Please provide a size on the command line (e.g., 5).\n";
    exit 1
  end;
  let c = ref 0
  and d = ref 0
  and v = ref 0 in
  let s = int_of_string Sys.argv.(1) in
  let n = 0 in
  (* Enumerate all closed terms of size [s]. *)
  term s n (fun t1 ->
    incr c;
    match step t1 with
    | None ->
        ()
    | Some t2 ->
        (* [t1] reduces to [t2]. *)
        incr d;
        let u1 = cpsinit t1
        and u2 = cpsinit t2 in
        if reduces u1 u2 then
          ()
        else begin
          (* [cps t1 init] does NOT reduce to [cps t2 init]. We have found
             a counter-example. *)
          incr v;
          printf "Here is a source term:\n";
          print_translation t1 u1;
          printf "This source term reduces to:\n";
          print_translation t2 u2;
          printf "SIMULATION VIOLATED!\n%!";
          printf "\n"
        end;
  );
  printf "There are %d terms of size %d with %d free variables.\n" !c s n;
  printf "Out of these, %d terms are capable of reducing.\n" !d;
  printf "Out of these, %d violations of simulation have been detected.\n" !v;
  ()
