open Printf
open CPS
open Common

(* -------------------------------------------------------------------------- *)

(* This little program generates lambda-terms and checks that the efficient
   and straightfoward versions of the CPS transformation both succeed and
   produce the same result. *)

let () =
  if Array.length Sys.argv < 2 then begin
    printf "Please provide a size on the command line (e.g., 5).\n";
    exit 1
  end;
  let c = ref 0 in
  let v = ref 0 in
  let s = int_of_string Sys.argv.(1) in
  let n = 0 in
  (* Enumerate all closed terms of size [s]. *)
  term s n (fun t ->
    incr c;
    let u1 = cpsinit t in
    let u2 = rcpsinit t in
    if u1 <> u2 then begin
      printf "Source term:\n";
      print t;
      printf "Translation:\n";
      print u1;
      printf "Wrong translation:\n";
      print u2;
      printf "\n%!";
      incr v
    end
  );
  printf "There are %d terms of size %d with %d free variables.\n" !c s n;
  printf "%d translation errors were detected.\n" !v;
  ()
