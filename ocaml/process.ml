(* We expect input data with two columns, namely [runs] and [nanos].
   [runs] is the run number, counting from 0 and up. (Run 0 is a
   dummy.) [nanos] is the total time since the beginning of the batch.
   We produce output data with one column, namely the time taken by
   each run. *)

let () =
  try
    let total = ref 0 in
    while true do
      let line = input_line stdin in
      let fields = String.split_on_char ' ' line in
      assert (List.length fields = 2);
      let field i = int_of_string (List.nth fields i) in
      let runs, nanos = field 0, field 1 in
      if runs = 0 then
        (* Reset the total time and omit this line. *)
        total := nanos
      else begin
        (* Compute the time difference since the previous run. *)
        let difference = nanos - !total in
        if difference >= 0 then begin
          total := nanos;
          Printf.printf "%d\n" difference
        end
        else begin
          Printf.eprintf "Anomaly: run %d, previous time %d, new time %d.\n"
            runs !total nanos
        end
      end
    done
  with End_of_file ->
    ()
