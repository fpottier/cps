open Printf
open CPS
open Common
module Command = Core.Command
module Bench = Core_bench.Std.Bench

(* This little program generates random lambda-terms and measures the
   performance of the efficient and straightforward versions of the CPS
   transformation. *)

(* -------------------------------------------------------------------------- *)

(* The number of random terms that are used in one test. *)

let number =
  25

(* A size-indexed test function. *)

let test size =
  (* Generate [number] random closed terms. *)
  printf "Generating %d random terms of size %d...%!" number size;
  let terms : term array =
    Array.init number (fun _ -> random size 0)
  in
  printf " done.\n%!";
  (* Return a closure. [Core_kernel.Staged.stage] is the identity. *)
  Core_kernel.Staged.stage (fun () ->
    Array.iter (fun t ->
      (* Convert every term to CPS form. *)
      ignore (rcpsinit t)
    ) terms
  )

(* -------------------------------------------------------------------------- *)

(* A list of sizes that we wish to try. *)

let min_size =
  1000.0

let max_size =
  1000.0 *. min_size

let rate =
  1.1

let rec sizes (s : float) : int list =
  if s < max_size then
    int_of_float s :: sizes (rate *. s)
  else
    []

let sizes =
  sizes min_size

(* -------------------------------------------------------------------------- *)

(* A function that runs all tests sequentially. *)

let sequential () =
  (* Submit this indexed test. *)
  let test : Bench.Test.t list = [
    Bench.Test.create_indexed
      ~name:"rcps"
      ~args:sizes
      test
  ] in
  (* Run this test and show interactive output. *)
  Command.run (Bench.make_command test)
  (* The results are also written to a file if -save is used. *)
  (* Just run [make bench], followed with [make data]. *)

(* -------------------------------------------------------------------------- *)

(* A function that runs all tests in parallel. *)

let nlogn (x : int) : float =
  let x = float_of_int x in
  x *. log10 x

let parallel () =
  let cores = Auxiliary.get_number_of_cores () in
  let cores = cores / 4 in (* avoid overloading the system *)
  printf "Using %d cores.\n" cores;
  Functory.Cores.set_number_of_cores cores;
  (* Functory.Control.set_debug true; *)
  flush stdout; flush stderr;
  let (_ : unit list) = sizes |> Functory.Cores.map ~f:(fun size ->
    printf "Running a test at size %d...\n%!" size;
    let name = sprintf "rcps_%d" size in
    (* Allot 5 seconds up to size 10000,
       then start spending more time for larger benchmarks. *)
    let allotted = max 5.0 (5.0 *. nlogn size /. nlogn 10000) in
    let run_config =
      Bench.Run_config.create
        ~verbosity:`Low
        ~time_quota:(Core.Time.Span.of_sec allotted) ()
    in
    let test =
      Bench.Test.create
        ~name
        (Core_kernel.Staged.unstage (test size))
    in
    let measurements =
      Bench.measure ~run_config [ test ]
    in
    List.iter (fun measurement ->
      Bench.Measurement.save measurement (name ^ "-.txt")
      ) measurements
  ) in
  ()

(* -------------------------------------------------------------------------- *)

let () =
  parallel()
