This directory contains several little OCaml programs:

* `make test` tests that the efficient and straightforward implementations of
  the CPS transformation appear to be equivalent. This is an exhaustive test,
  on closed lambda-terms of bounded size.

* `make bench` measures the performance of the efficient CPS implementation
  on randomly-generated lambda-terms of increasing sizes.
  The measurements are exploited by subsequently running `make data` and
  `make plot`.

* `make enum` searches for a counter-example to the simple simulation diagram.
  This is an exhaustive test, on closed lambda-terms of bounded size.
