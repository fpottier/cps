let fail format =
  Printf.kprintf failwith format

let try_finally f h =
  let result = try f() with e -> h(); raise e in
  h(); result

let with_process_code_result command (f : in_channel -> 'a) : int * 'a =
  let ic = Unix.open_process_in command in
  set_binary_mode_in ic false;
  match f ic with
  | exception e ->
      ignore (Unix.close_process_in ic); raise e
  | result ->
      match Unix.close_process_in ic with
      | Unix.WEXITED code ->
          code, result
      | Unix.WSIGNALED _
      | Unix.WSTOPPED _ ->
          99 (* arbitrary *), result

let with_process_result command (f : in_channel -> 'a) : 'a =
  let code, result = with_process_code_result command f in
  if code = 0 then
    result
  else
    fail "Command %S failed with exit code %d." command code

let get_number_of_cores () =
  try match Sys.os_type with
  | "Win32" ->
      int_of_string (Sys.getenv "NUMBER_OF_PROCESSORS")
  | _ ->
      with_process_result "getconf _NPROCESSORS_ONLN" (fun ic ->
        let ib = Scanf.Scanning.from_channel ic in
        Scanf.bscanf ib "%d" (fun n -> n)
      )
  with
  | Not_found
  | Sys_error _
  | Failure _
  | Scanf.Scan_failure _
  | End_of_file
  | Unix.Unix_error _ ->
      1
