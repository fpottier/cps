(** val add : int -> int -> int **)

let rec add = (+)

(** val sub : int -> int -> int **)

let rec sub = fun n m -> Pervasives.max 0 (n-m)

type var = int

(** val funcomp : ('a1 -> 'a2) -> ('a2 -> 'a3) -> 'a1 -> 'a3 **)

let funcomp f g x =
  g (f x)

(** val scons : 'a1 -> (var -> 'a1) -> var -> 'a1 **)

let scons s sigma x =
  (fun fO fS n -> if n=0 then fO () else fS (n-1))
    (fun _ ->
    s)
    (fun y ->
    sigma y)
    x

(** val lift : var -> var -> var **)

let lift x y =
  add x y

type ('t1, 't2) _bind = 't2

type 'term ids = var -> 'term

(** val ids0 : 'a1 ids -> var -> 'a1 **)

let ids0 ids1 =
  ids1

type 'term rename = (var -> var) -> 'term -> 'term

(** val rename0 : 'a1 rename -> (var -> var) -> 'a1 -> 'a1 **)

let rename0 rename1 =
  rename1

type 'term subst = (var -> 'term) -> 'term -> 'term

(** val subst0 : 'a1 subst -> (var -> 'a1) -> 'a1 -> 'a1 **)

let subst0 subst1 =
  subst1

(** val ren : 'a1 ids -> (var -> var) -> var -> 'a1 **)

let ren h xi =
  funcomp xi (ids0 h)

(** val up : 'a1 ids -> 'a1 rename -> (var -> 'a1) -> var -> 'a1 **)

let up h h0 sigma =
  scons (ids0 h 0) (funcomp sigma (rename0 h0 (lift (Pervasives.succ 0))))

(** val upren : (var -> var) -> var -> var **)

let upren xi =
  scons 0 (funcomp xi (fun x -> Pervasives.succ x))

type term =
| Var of var
| Lam of (term, term) _bind
| App of term * term
| Let of term * (term, term) _bind

(** val ids_term : term ids **)

let ids_term h =
  Var h

(** val rename_term : term rename **)

let rec rename_term xi = function
| Var x -> Var (xi x)
| Lam t -> Lam (rename0 rename_term (upren xi) t)
| App (s1, s2) ->
  App ((rename0 rename_term xi s1), (rename0 rename_term xi s2))
| Let (s0, t2) ->
  Let ((rename0 rename_term xi s0), (rename0 rename_term (upren xi) t2))

(** val subst_term : term subst **)

let rec subst_term sigma = function
| Var x -> sigma x
| Lam t -> Lam (subst0 subst_term (up ids_term rename_term sigma) t)
| App (s1, s2) ->
  App ((subst0 subst_term sigma s1), (subst0 subst_term sigma s2))
| Let (s0, t2) ->
  Let ((subst0 subst_term sigma s0),
    (subst0 subst_term (up ids_term rename_term sigma) t2))

type continuation =
| O of term
| M of term

(** val apply : continuation -> term -> term **)

let apply c v =
  match c with
  | O k -> App (k, v)
  | M k -> subst0 subst_term (scons v (ids0 ids_term)) k

(** val reify : continuation -> term **)

let reify = function
| O k -> k
| M k -> Lam k

(** val substc : (var -> term) -> continuation -> continuation **)

let substc sigma = function
| O k -> O (subst0 subst_term sigma k)
| M k -> M (subst0 subst_term (up ids_term rename_term sigma) k)

(** val cps : term -> continuation -> term **)

let rec cps x =
  let cps_ = fun y -> cps y in
  (fun c ->
  match x with
  | Var x0 -> apply c (Var x0)
  | Lam t ->
    apply c (Lam (Lam
      (cps_ (subst0 subst_term (ren ids_term (lift (Pervasives.succ 0))) t)
        (O (Var 0)))))
  | App (t1, t2) ->
    cps_ t1 (M
      (cps_ (subst0 subst_term (ren ids_term (lift (Pervasives.succ 0))) t2)
        (M (App ((App ((Var (Pervasives.succ 0)), (Var 0))),
        (subst0 subst_term
          (ren ids_term (lift (Pervasives.succ (Pervasives.succ 0))))
          (reify c)))))))
  | Let (t1, t2) ->
    cps_ t1 (M (Let ((Var 0),
      (cps_
        (subst0 subst_term
          (up ids_term rename_term (ren ids_term (lift (Pervasives.succ 0))))
          t2)
        (substc (ren ids_term (lift (Pervasives.succ (Pervasives.succ 0))))
          c))))))

(** val init : continuation **)

let init =
  M (Var 0)

(** val cpsinit : term -> term **)

let cpsinit t =
  cps t init

type map = int IntMap.t

(** val empty : unit -> map **)

let empty = (fun () -> IntMap.empty)

(** val add0 : int -> int -> map -> map **)

let add0 = IntMap.add

(** val find : int -> map -> int **)

let find = IntMap.find

type renaming = (int * map) * int

(** val _id : unit -> renaming **)

let _id _ =
  ((0, (empty ())), 0)

(** val _up : renaming -> renaming **)

let _up = function
| (p, dst) ->
  let (src, m) = p in
  (((add (Pervasives.succ 0) src), (add0 src dst m)),
  (add (Pervasives.succ 0) dst))

(** val interpret : renaming -> var -> var **)

let interpret rho x =
  let (p, dst) = rho in
  let (src, m) = p in
  if (<) x src
  then let x0 = sub (sub src (Pervasives.succ 0)) x in
       let y = find x0 m in sub (sub dst (Pervasives.succ 0)) y
  else sub (add x dst) src

type rterm = int -> term

(** val var0 : int -> int -> rterm **)

let var0 dst i =
  let i0 = sub (sub dst (Pervasives.succ 0)) i in
  (fun dst' -> let i1 = sub (sub dst' (Pervasives.succ 0)) i0 in Var i1)

(** val lambda : (rterm -> rterm) -> rterm **)

let lambda t dst =
  Lam
    (let dst0 = add (Pervasives.succ 0) dst in
     let x = var0 dst0 0 in t x dst0)

(** val app : rterm -> rterm -> rterm **)

let app t1 t2 dst =
  App ((t1 dst), (t2 dst))

(** val svar : var -> (int * map) -> rterm **)

let svar x env dst =
  Var (interpret (env, dst) x)

(** val sbind : ((int * map) -> int -> term) -> (int * map) -> rterm **)

let sbind t env dst =
  let (env0, dst0) = _up (env, dst) in t env0 dst0

(** val slambda : (int * map) -> ((int * map) -> int -> term) -> rterm **)

let slambda env t dst =
  Lam (sbind t env dst)

(** val slet :
    (int * map) -> (int -> term) -> ((int * map) -> int -> term) -> rterm **)

let slet env t1 t2 dst =
  Let ((t1 dst), (sbind t2 env dst))

type rcontinuation =
| RO of rterm
| RM of (rterm -> rterm)

(** val rapply : rcontinuation -> rterm -> rterm **)

let rapply c v =
  match c with
  | RO k -> app k v
  | RM k -> k v

(** val rreify : rcontinuation -> rterm **)

let rreify = function
| RO k -> k
| RM k -> lambda k

(** val rcps : (int * map) -> term -> rcontinuation -> rterm **)

let rec rcps env t c =
  match t with
  | Var x -> rapply c (svar x env)
  | Lam t0 ->
    rapply c
      (slambda env (fun env0 -> lambda (fun k -> rcps env0 t0 (RO k))))
  | App (t1, t2) ->
    rcps env t1 (RM (fun v1 ->
      rcps env t2 (RM (fun v2 -> app (app v1 v2) (rreify c)))))
  | Let (t1, t2) ->
    rcps env t1 (RM (fun v1 -> slet env v1 (fun env0 -> rcps env0 t2 c)))

(** val rinit : rcontinuation **)

let rinit =
  RM (fun t -> t)

(** val rcpsinit : term -> term **)

let rcpsinit t =
  let (env, dst) = _id () in rcps env t rinit dst
