\section{A one-pass call-by-value CPS transformation --- informal version}
\label{sec:informal}

\input{figures-informal}

\newcommand{\footnotedanvynielsen}{%
\footnote{We closely follow Danvy and Filinski's
higher-order one-pass formulation~\citeyear[Figure~3]{danvy-filinski-92}.
However, whereas Danvy and Filinski define two functions
$\llbracket\cdot\rrbracket'$ and $\llbracket\cdot\rrbracket$,
which respectively expect a term-level continuation and a
metalevel continuation, we define a single function~$\cps\cdot\cdot$,
whose second argument is either an object-level continuation or a metalevel
continuation. This eliminates a significant amount of duplication in the
definition. The existence of such a formulation was known to Danvy and
Filinski~\citeyear[\S2.6]{danvy-filinski-92}, who write that
``[one could] instrument the translation with an inherited attribute
identifying tail-call contexts''.}\xspace}

We consider a core \lc whose terms are
$ t ::= x \mid \Lam{x.t} \mid \App t t \mid \Let {x=t} t $.
A value~$v$ (also, $k$) is a term of the form $x$ or $\Lam{x.t}$.
%
We wish to define two metalevel functions, namely:
\[\begin{array}{r@{\qquad}l}
\cpsv v & \text{the CPS transformation of the value~$v$} \\
\cps tc & \text{the CPS transformation of the term~$t$ with continuation~$c$} \\
\end{array}\]
The latter form can be read informally as ``with the result of evaluating~$t$,
do~$c$''. We let a continuation~$c$ be either a term $\objkont$, also known as
an object-level continuation, or a metalevel function $\metakont$ of terms to
terms, also known as a metalevel continuation.\footnotedanvynielsen
%
The purpose of this distinction is to avoid the construction of so-called
administrative redexes~\cite[\S1]{danvy-filinski-92}.
%
We use explicit injections and write
$c ::= \Tail\objkont \mid \NonTail\metakont$. The injections $\Tail\cdot$ and
$\NonTail\cdot$, although noisy, are necessary (even on paper) to avoid
ambiguity.
%
% Especially when we switch to a first-order representation, where
% object-level and metalevel continuations are represented as terms.
% There, we find that "o" is not a binder and "m" is a binder.
%

Throughout the paper, we write $\metalam vt$ for a metalevel abstraction
(``the function that maps $v$ to~$t$'') and $\metaapp\metakont{v}$ for
a metalevel application (``the result of applying~$\metakont$ to~$v$'').
%
A~metalevel function $\metalam vt$ can be intuitively thought of as a
term $t$ with a hole named~$v$. A~metalevel application $\metaapp tv$
can be thought of as the term obtained by filling the hole in~$t$ with
the value~$v$.
%
In \sref{sec:efficient}, we write $\metalet v{t_1}{t_2}$ for a metalevel
local definition.
% The names of the metavariables do not quite make sense here,
% but never mind.

Two key operations on continuations are $\applyname$ and $\reifyname$
(\fref{fig:continuations:informal}). The term $\apply cv$ can be thought of as
the application of the continuation~$c$ to the value~$v$. This is either an
object-level application or a metalevel application, depending on the nature
of~$c$. The term $\reify c$ can be thought of as the continuation~$c$,
represented as a term. If $c$ is an object-level continuation~$\Tail\objkont$, then
$\reify c$ is just $\objkont$. If $c$ is a metalevel
continuation~$\NonTail\metakont$, then $\reify c$ is $\Lam{x.(\metaapp\metakont x)}$,
that is, a $\lambda$-abstraction whose formal parameter is a fresh
variable~$x$ and whose body is obtained by applying $\metakont$ to the term
$x$.
% This is known in the literature as a two-level $\eta$-expansion.

The call-by-value CPS transformation is defined in \fref{fig:cps:informal}.
The functions $\cpsv\cdot$ and $\cps\cdot\cdot$ are defined in a mutually
inductive manner.

\newcommand{\footnoteparams}{\footnote{%
Ideally, the target language of the translation would have functions of
arity 2. For simplicity, we prefer to unify the source and target languages,
and represent a function of arity 2 as a curried function.}\xspace}

Equations~1 and~2 define the translation $\cpsv v$ of a value~$v$. A variable
is translated to itself, while a function of one parameter~$x$ is translated
to a function of two parameters\footnoteparams~$x$ and~$y$, where the fresh variable~$y$
stands for a continuation. The function body, $t$, is translated with
object-level continuation~$y$.

The remaining equations define the translation $\cps tc$ of a term~$t$ under a
continuation~$c$. Equation~3 states that if the term~$t$ happens to be a
value~$v$, then its translation $\cps vc$ is the application of the
continuation~$c$ to the value~$\cpsv v$. Equation~4 defines the term
$\cps{\App{t_1}{t_2}}c$ so that the translation of $t_1$ runs first, yielding
a value denoted by the metavariable~$v_1$. Then, the translation of~$t_2$ is
executed, yielding a value denoted by~$v_2$. Finally, an object-level
application of $v_1$ to the two arguments~$v_2$ and $\reify c$ is built, in
accordance with the fact that a translated function takes two arguments: a
value and a continuation. In equation~5, the translation of the term~$t_1$ is
executed first. Its value, denoted by~$v_1$, is bound via a \kw{let}
construct to the variable~$x$. The continuation~$c$ is used, unchanged, in the
translation of $t_2$, reflecting the fact that $t_2$ occurs in tail position
in the term $\Let{x=t_1}{t_2}$.

% The above material is informal: it does not appear in our Coq formalization.
