\section{An efficient implementation of the CPS transformation}
\label{sec:efficient}

Extracting OCaml code out of the Coq definitions of
Figures~\ref{fig:continuations} and~\ref{fig:cps} yields a working yet very
inefficient implementation of the CPS transformation. Indeed, these
definitions involve substitution under various guises. A substitution
operation $\subst \metakont {\singleton v}$ is explicitly used in the
definition of $\applyname$. The lifting operations $\liftop1$, $\liftop2$,
and $\upplus$ are also substitution operations.
%
This gives rise to two problems. First, even if the operation of applying a
substitution to a term was somehow efficiently implemented, its cost would
still be at least linear in the size of the term, which implies that the time
complexity of the CPS transformation would be at least quadratic.
%
% In fact, the call $\apply c{...}$ worries me,
% as its cost would be linear in the size of the *continuation*,
% not in the size of the term.
% The overall complexity might be worse than quadratic,
% even under the assumption that substitution has linear time complexity.
%
Second, Autosubst's implementation of this operation is inefficient. Indeed,
for mathematical simplicity, Autosubst represents a substitution~$\sigma$ as a
function. From an algorithmic point of view, this is not a good choice. The
construction of the substitution~$\up\sigma$, which takes place when a binder
is entered, allocates a new closure. Thus, a substitution forms a linked list
of closures in memory, whose length is at least the number of binders that
have been entered. Therefore, the cost of applying a substitution to a
variable is at least linear, and the cost of applying a substitution to a term
is at least quadratic.

This naturally raises the questions: can the CPS transformation, viewed as a
function of terms (in de Bruijn's representation) to terms (in the same
representation), be efficiently implemented? Can this implementation be proved
correct with respect to the definition that we have presented earlier
(\sref{sec:formal})? Because we found this ``programming exercise''
considerably more difficult than expected, we describe our solution.

% An efficient implementation would probably be reasonably straightforward in
% the nominal representation (at the cost of explicit reasoning about
% freshness). At worst, we could translate to the nominal representation, CPS
% transform, and translate back to de Bruijn's representation, but that sounds
% silly, and the proof of correctness of such an indirect scheme with respect
% to our de-Bruijn-to-de-Bruijn specification would probably be quite involved.

% Thus, we strive to give a reasonably direct formulation of a de Bruijn-to-de
% Bruijn transformation. This turns out to be a rather difficult programming
% exercise. Our solution is slightly indirect as it implicitly goes through de
% Bruijn levels, but that remains mostly under the rug.

In order to obtain an efficient implementation of the CPS transformation, it
is necessary to avoid the use of lifting and substitution altogether. To avoid
the substitution $\subst \metakont {\singleton v}$ of a value $v$ into a
term-with-a-hole $\metakont$, we revert to a higher-order formulation, where
metalevel continuations are represented as metalevel functions. To avoid the
lifting operations $\liftop1$, $\liftop2$, and $\upplus$, we explicitly carry
and maintain a renaming, which is applied on the fly.
% Lifting and transformation are fused.
Of course, an efficient representation of renamings, which supports
lookup and extension in logarithmic time, must be used.

% ------------------------------------------------------------------------------

\subsection{Efficient de Bruijn renamings}
\label{sec:efficient:renamings}

To begin with, we need an efficient representation of renamings. Recall that a
renaming is a total mapping of variables to variables, or more precisely, of
de Bruijn indices to de Bruijn indices. This representation must efficiently
support the following four operations:
%
\begin{enumerate}
\item Build the identity renaming $\ids$.
\item Given a renaming~$\sigma$ and a variable~$x$,
      compute $\sigma(x)$.
\item Given a renaming~$\sigma$,
      construct the renaming $\up\sigma$.
\item Given a renaming~$\sigma$,
      construct the renaming $\sigma\scomp(\plus1)$.
\end{enumerate}
%
It should be obvious why the first two operations are needed. Operation~3 is
required when a ($\lambda$-bound or \kw{let}-bound) variable in the source
program is translated to a (similarly $\lambda$-bound or \kw{let}-bound)
variable in the target program. Operation~4 is required when a variable that
does not exist in the source program is introduced in the target program: this
is the case of continuation variables.

To gain an intuition for these operations, it is useful to visualize a
renaming~$\sigma$ as a bipartite graph on $\mathbb N\times\mathbb N$, where the
source variables, labeled $0, 1, \ldots$, appear on the left-hand side; the target
variables, labeled $0, 1, \ldots$, appear on the right-hand side; and where
there is an edge from
every source variable~$x$ to the target variable $\sigma(x)$. Operation~3 can
then be viewed as renumbering every vertex on either side by adding 1 to its
label; creating one new vertex, numbered 0, on either side; and adding a new
edge between these new vertices. Visually, the original graph is untouched;
two new vertices and a new edge are added to it. Operation~4 can be viewed as
renumbering every vertex on the right-hand side by adding 1 to its label, and
creating one new vertex, numbered 0, on the right-hand side. Visually, the
original graph is again untouched; one new vertex is added to it.

It may not be obvious at first how these four operations can be efficiently
implemented. A~traditional finite map data structure, such as a balanced
binary search tree, supports the insertion of new key-value pairs, but does
not support renumbering every key in its domain or renumbering every value in
its codomain. A binary random access list~\cite[Chapter~9]{okasaki-book-99}
\cite{sozeau-finger-07} supports the former operation, but not the latter.

Our solution to this puzzle is to internally represent a renaming using a
finite map~$m$ of de Bruijn levels to de Bruijn levels. (de Bruijn levels are
analogous to de Bruijn indices, but are counted in the opposite direction:
a~higher level denotes a more recent binder.) Let us assume, for a moment,
that the height of the source name space is $\src$, that is, only the source
variables in the semi-open interval $[0, \src)$ are of interest. Then, the
conversion between a source de Bruijn index and a source de Bruijn level (in
either direction) is performed by transforming $x$ to $\src - 1 - x$.
Similarly, if the height of the target name space is $\dst$, then the
conversion between a target index and a target level is performed by
transforming $x'$ to $\dst - 1 - x'$. Thus, if $m$ is a map of levels to
levels, and if the operation $\find xm$ can be used to look up $x$ in this
map, then the corresponding transformation of indices to indices
maps~$x$ to $ \dst - 1 - \find{(\src - 1 - x)}m $.

Formally speaking, let us assume that a correct and efficient implementation
of finite maps is available. We assume that this data structure supports the
operations $\vide$, $\addname$, and $\findname$. (We axiomatize these
operations in Coq and implement them in OCaml using
OCaml's balanced binary search tree library.) Then, we represent a renaming as
a triple $(\src, m, \dst)$, where $m$ is a finite map and $\src$ and $\dst$
are natural numbers. (Slightly cutting corners, we instruct Coq to represent
natural numbers as OCaml machine integers.)
%
The function $\interpretname$ in \fref{fig:interpret} defines how a triple
$(\src, m, \dst)$ should be interpreted as a renaming, that is, as a total
function of variables to variables.
%
The first equation in \fref{fig:interpret} has been justified above. The
second equation in \fref{fig:interpret} causes the renaming to behave
uniformly beyond the heights of the source and target name spaces: that is,
the variables $\src, \src+1, \ldots$ are mapped to $\dst, \dst+1, \ldots$,
and so on.

\begin{definition}% [Representation of renamings]
  A triple $(\src, m, \dst)$ \define{represents} a renaming~$\sigma$ if
  \begin{enumerate}
  \item $m$ is a well-formed finite map;
  \item the interval $[0\ldots\src)$ is contained in the domain of~$m$;
  \item the image of this interval through $m$ is contained in the interval $[0\ldots\dst)$;
  \item for every~$x$, the equality $\Var{\interpret{(\src, m, \dst)}x} = \sigma(x)$ holds.
  \end{enumerate}
\end{definition}
%
\begin{lemma}
The four desired operations on renamings are implemented as follows:
\begin{enumerate}
\item $(0, \vide, 0)$ represents the identity renaming.
\item If $(\src, m, \dst)$ represents $\sigma$,
      then $\interpret{(\src, m, \dst)}x$ is $\sigma(x)$.
\item If $(\src, m, \dst)$ represents $\sigma$,
      then $(\src + 1, \add\src\dst m, \dst + 1)$ represents $\up\sigma$.
\item If $(\src, m, \dst)$ represents $\sigma$,
      then $(\src, m, \dst + 1)$ represents $\sigma\scomp(\plus1)$.
\end{enumerate}
\end{lemma}

% ------------------------------------------------------------------------------

\subsection{Relocatable terms}

In order to obviate the need for lifting operations, such as $\liftop1$,
instead of constructing terms, whose free variables might later need
to be renumbered, we construct relocatable terms. A~\define{relocatable term}
is a function, which, when applied to the height $\dst$ of the target name
space, produces a term. Thus, the type ``$\tyrterm$'' of relocatable terms is
defined as $\tynat \tyar \tyterm$.

% It could be argued that this is really a way of performing the transformation
% in two passes: first, translate to a term in de Bruijn level representation;
% then, translate to a term in de Bruijn index representation. However, I am
% not entirely sure that this intuition is correct, so I omit it.

In the following,
we reuse the metavariables~$t, k, v$ to range over relocatable terms, as
it would be cumbersome to have to invent a whole new set of metavariables. The
type of a metavariable should always be clear from the context, as in the
next definition, where $t$~denotes a~relocatable term and $t'$ denotes
a~term. Similarly, in the next subsection (\sref{sec:rcont}), the
metavariable~$\rmetakont$ is reused to denote a metalevel function of
relocatable terms to relocatable terms, and the metavariable $\rc$ is reused
to denote a relocatable continuation.

The meaning of a relocatable term is defined by a 3-place predicate whose
parameters respectively have types $\tyrterm$, $\tyterm$, and $\tynat$, as
follows.

\begin{definition}
\label{def:represents:rterm}
  A relocatable term $t$ \define{represents} a term $t'$ \define{at time} $\dst$
  if and only if, for every $i$,
  the application
  $\metaapp{t}{(\dst + i)}$
  yields the term
  $\lift i {t'}$.
\end{definition}

This definition builds in the idea that the height of the target name space
can only grow between the time~$\dst$ when a relocatable term is constructed and
the time $\dst+i$ when this relocatable term is applied so as to obtain a
term.

The following (trivial) lemma states that relocatable terms really are
relocatable: that is, they do not need to be explicitly lifted. If the
relocatable term~$t$ represents the term $t'$ when the height of the target
name space is $\dst$, then the same relocatable term $t$ automatically
represents $\lift i{t'}$ when the height of the target name space grows to
$\dst + i$.

\begin{lemma}[Relocation]
\label{lemma:relocation:rterm}
  If $t$ represents $t'$ at time $\dst$,
  then $t$ represents $\lift i{t'}$ at time $\dst + i$.
\end{lemma}

We use the auxiliary functions in \fref{fig:rterm} to construct relocatable
terms.
% One may wonder why there isn't a function for constructing a let.
% It so happens that we do not need it.

The function call $\rvar\dst x$ converts the de Bruijn index~$x$ to a de
Bruijn level $\dst-1-x$ and returns a function which, when applied to $\dst'$,
converts this level back to an index. It is expected that $\dst'$ is greater
than or equal to $\dst$.
% as the height of the target name space can only grow between the time when
% $\rvar\dst x$ is invoked and the time when the resulting relocatable term is
% applied so as to obtain a term.
The idea is to exploit the fact that de Bruijn levels are unaffected by the
growth of the name space that they inhabit.

The relocatable term $\rlambda t$, once instructed of the height $\dst$ of the
target name space, increases $\dst$ by 1, so as to make room for one more
variable, applies the function~$t$ to a relocatable representation of the
variable~0, and applies the resulting relocatable term to $\dst$, so as to
obtain a term.

The identity function is represented by the relocatable term
$\rlambda{(\metalam xx)}$, which (as can be checked by unfolding)
is a constant function which ignores its argument $\dst$ and
always returns the term $\Lam{\Var 0}$.

The representation predicate of Definition~\ref{def:represents:rterm} can be
used to give specifications to the functions $\rvarname$, $\rlambdaname$, and
$\rappname$. For instance, provided $x < \dst$ holds, the relocatable term
$\rvar\dst x$ represents the term $x$ at time~$\dst$. We omit the
specifications of $\rlambdaname$ and $\rappname$.

% There is a certain white lie here, as we do not quite have a satisfactory
% specification of $\rlambdaname$. The specification lemma that we prove is
% used once, but there is another use site where we just expand $\rlambdaname$
% away, because the specification lemma appears unusable. This unsatisfactory
% aspect is discussed in the conclusion.

% ------------------------------------------------------------------------------

\input{figures-efficient}

% ------------------------------------------------------------------------------

\subsection{Relocatable continuations}
\label{sec:rcont}

In our initial informal presentation of the CPS transformation
(\sref{sec:informal}), we have used a higher-order style, where
terms-with-a-hole are represented as metalevel functions. Then
(\sref{sec:formal}), we have switched to a first-order style,
where they are represented as syntax, because this
style is easier to reason about. We now switch back to a
higher-order style, because representing a term-with-a-hole as
a metalevel function is efficient. The
operation of filling the hole with a value is just metalevel
function application; there is no need for a substitution
operation. Provided this operation is
ever performed at most once, this approach is profitable.
% If the hole was filled twice,
% the term-with-a-hole would be constructed twice.
% That would be inefficient.
In the CPS transformation,
every term-with-a-hole
is filled exactly once,
so this condition is satisfied.

A \define{relocatable continuation}~$\rc$ is either a relocatable term $\robjkont$ (of
type $\tyrterm$) or a metalevel function $\rmetakont$ (of type $\tyrterm \tyar
\tyrterm$). We write $\rc ::= \RTail\robjkont \mid \RNonTail\rmetakont$.
We write ``$\tyrcont$'' for the type of relocatable continuations.

The meaning of a relocatable continuation is made explicit by a 3-place predicate
whose parameters respectively have types $\tyrcont$, $\tycont$, and $\tynat$,
as follows.

\begin{definition}
  The assertion that a relocatable continuation~$\rc$
  \define{represents}
  a continuation~$c'$ \define{at time} $\dst$
  is defined by the following two rules:
  \begin{enumerate}
  \item If $\robjkont$ represents $\objkont'$ at time $\dst$,
        then $\RTail\robjkont$ represents $\Tail{\objkont'}$ at time $\dst$.
  \item If, for all $i$, for all values $v$ and $v'$
            such that $v$ represents $v'$ at time $\dst + i$, \\
        \phantom{If, for all $i$,} $\metaapp\rmetakont v$ represents
            $\subst{\metakont'}{\scons{v'}{(\plus i)}}$, \\
        then $\RNonTail\rmetakont$ represents $\NonTail{\metakont'}$ at time $\dst$.
  \end{enumerate}
\end{definition}

% The substitution operation $\subst{\metakont'}{\scons{v'}{(\plus i)}}$
% substitutes the value $v'$ for the hole~$0$ and lifts every free variable of
% $\NonTail{\metakont'}$ up by $i$.

We do not attempt to explain in detail this admittedly slightly cryptic
definition. Let us just state the following two lemmas, which confirm that
everything works as expected.
%
Lemma~\ref{lemma:relocation:rcont} states that relocatable continuations are
indeed relocatable. (Its statement has the same structure as that of
Lemma~\ref{lemma:relocation:rterm}.)
%
Lemma~\ref{lemma:rapply:rreify} states that the operations $\rapplyname$ and
$\rreifyname$, defined in \fref{fig:continuations:efficient}, are correct
implementations of $\applyname$ and $\reifyname$.
%
\begin{lemma}[Relocation]
\label{lemma:relocation:rcont}
  If $\rc$ represents $c'$ at time $\dst$,
  then $\rc$ represents $\liftc i{c'}$ at time $\dst + i$.
\end{lemma}

\begin{lemma}
\label{lemma:rapply:rreify}
  If $\rc$ represents $c'$ at time $\dst$, then:
  \begin{enumerate}
  \item If $v$ represents $v'$ at time $\dst$,
        then $\rapply\rc v$ represents $\apply{c'}{v'}$ at time $\dst$.
  \item $\rreify\rc$ represents $\reify{c'}$ at time $\dst$.
  \end{enumerate}
\end{lemma}

% ------------------------------------------------------------------------------

\subsection{An efficient implementation of the CPS transformation}

An efficient formulation of the CPS transformation is given in
\fref{fig:cps:efficient}. The value transformation,
written $\rcpsv \env v$, and the term transformation,
written $\rcps \env tc$, are defined in a mutually recursive manner.
% (This is structural recursion.)
%
We write $\env$ for a pair $(\src, m)$,
and we write $\tyenv$ for the type of such a pair.
%
The type of the value transformation function $\rcpsv\cdot\cdot$ is
$\tyterm \tyar \tyenv \tyar \tyrterm$, which means that this function must be
successively applied to a source value~$v$, to a pair $(\src, m)$, and to a
number $\dst$, producing a transformed term. Together, $(\src, m, \dst)$
represent a renaming (\sref{sec:efficient:renamings}), that is, a total
function of variables in the source name space to variables in the target name
space. Yet, things have been arranged so that $(\src, m)$ and $\dst$ are
passed separately, in two distinct phases; the partial application
$\rcpsv\env v$ is a relocatable term.

We do not explain the definition in detail. Let us note that $\sourcevarname$
applies the renaming $(\src, m, \dst)$ to a source variable, yielding a target
variable. $\sourcebindname$ is used when a binder in the source term gives
rise to a binder in the target term; then, the current renaming~$\sigma$
becomes $\up\sigma$. This is the case in $\sourcelambdaname$ and
$\sourceletname$, which are used when a $\lambda$ or \kw{let} binder in the
source term gives rise to a $\lambda$ or \kw{let} binder in the target term.

The following lemma states that this formulation of the CPS transformation is
equivalent to our earlier formulation (\sref{sec:formal}).
% Perhaps surprisingly,
Its proof, by induction on the term $t$, is not difficult.

\begin{lemma}[Correctness]
\label{lemma:efficient:correctness}
  If $(\src, m, \dst)$ represents $\sigma$ and
  if $\rc$ represents $c'$ at time $\dst$,
  then the relocatable term $\rcps{(\src, m)}{t}\rc$
  represents the term $\cps{\subst t\sigma}{c'}$ at time $\dst$.
\end{lemma}

As a corollary, we find that the two formulations of the CPS transformation
coincide.

\begin{lemma}[Coincidence]
  Let us write $\env_0$ for the initial environment $(0, \vide)$
  and $\rinit$ for the empty-context relocatable continuation
  $\ridc$.
  Then, for every term $t$,
  the terms $\metaapp{\rcps{\env_0}{t}{\rinit}}0$
  and $\cps{t}\init$ coincide.
\end{lemma}

% How does this code work, intuitively?
% I would be hard pressed to answer.
% Intuitively, a first pass builds an rterm,
% which is a CPS-transformed term in de-Bruijn-level representation.
% Then, when this rterm is applied to dst, in a second pass,
% a CPS-transformed term in de-Bruijn-index representation is produced.
% But the two passes are really intertwined,
% because sbind/slambda/slet don't do anything
% until they receive their last argument dst.
% It is difficult to understand exactly which actions
% are executed in which order.

This formulation of the CPS transformation, whose complete code appears in
Figures~\ref{fig:interpret} to~\ref{fig:cps:efficient}, is efficient. We argue
informally that its worst-case time complexity is $O(n\log n)$, where $n$~is
the size of the source term. Indeed, no lifting or substitution operations
whatsoever are involved. The dictionary operations $\findname$ and $\addname$
have worst-case time complexity $O(\log n)$. Every other elementary operation,
including allocations of abstract syntax tree nodes and closure allocations,
has worst-case time complexity $O(1)$. Finally, every closure is invoked
exactly once, so the cost of executing this closure can be charged to the site
where this closure is allocated. It should then be evident that the worst-case
time complexity of the transformation is $O(n\log n)$.
%
This claim would arguably deserve a formal proof,
which we leave to future work!

We have extracted OCaml code from this definition and have experimentally
confirmed that the time complexity of the transformation, applied to randomly
generated closed terms, seems to be $\Theta(n\log n)$. Indeed, the running
time (measured by Jane Street's \coq|core_bench| library, which attempts to
even out the effect of GC), divided by $n\log n$, seems to give rise to a
constant function of $n$ (\fref{fig:experiment}).
%

\begin{figure}[t]
\includegraphics[width=.5\textwidth]{../ocaml/plot-balanced-trees.pdf}
\includegraphics[width=.5\textwidth]{../ocaml/plot-skewed-trees.pdf}
\caption{Running time divided by $n\log n$, as a function of $n$.
  The horizontal scale is logarithmic.
  At left, random terms;
  at right, ``right-leaning'' random terms, whose \kw{let} constructs
  have a left-hand side of bounded size.}
\label{fig:experiment}
\end{figure}
