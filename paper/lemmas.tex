\section{Basic lemmas}
\label{sec:lemmas}

We now state three basic lemmas that describe how the CPS transformation
interacts with renamings and substitutions. The three statements have similar
structure. All three of them are proved in the same manner, namely by mutual
induction over the size of~$v$ and~$t$, using the mutual induction principle
mentioned above. The proofs are not difficult: for each lemma, a proof script
of about 25 nonblank, noncomment lines is required.

% ------------------------------------------------------------------------------

The CPS transformation commutes with renamings: in short, a renaming~$\sigma$
can be pushed down into both sides of $\cps tc$.

\begin{lemma}[Renaming]
\label{lemma:renaming}
  For every renaming~$\sigma$,
  \begin{enumerate}
  \item for every value $v$,
        $\subst{\cpsv v}\sigma = \cpsv{\subst v\sigma}$.
  \item for every term $t$ and continuation $c$,
        $\subst{(\cps tc)}\sigma = \cps{\subst t\sigma}{\substc c\sigma}$.
  \end{enumerate}
\end{lemma}

As an almost-immediate corollary, we obtain the following equality:
%
\[ \up{(\sigma \funcomp \cpsv\cdot)} = (\up\sigma) \funcomp \cpsv\cdot \]
%
That is, if a substitution $\sigma'$ can be expressed as the composition
$\sigma \funcomp \cpsv\cdot$ of a substitution $\sigma$ and the CPS
transformation, then $\up{\sigma'}$ can be similarly expressed as the
composition $(\up\sigma) \funcomp \cpsv\cdot$. This equality is exploited in
the proof of the substitution lemma, which follows. This seems to be the
reason why we must establish Lemma~\ref{lemma:renaming} before
Lemma~\ref{lemma:substitution}, even though Lemma~\ref{lemma:substitution}
subsumes Lemma~\ref{lemma:renaming}.

% ------------------------------------------------------------------------------

The CPS transformation commutes with substitutions~$\sigma'$ that can be
expressed as a composition $\sigma \funcomp \cpsv\cdot$, where $\sigma$ is a
value substitution. In short, such a substitution~$\sigma'$ can be pushed down
into both sides of $\cps tc$, becoming $\sigma$ on the term side and remaining
$\sigma'$ on the continuation side. This reflects the fact that~$t$ represents
a source term, which is subject to the transformation, whereas~$c$ represents
a target term, which is not subject to it.
%
This result corresponds to
Danvy and Filinski's Lemma~2 \citeyear{danvy-filinski-92}
and to the first part of Danvy and Nielsen's
Lemma~1~\citeyear{danvy-nielsen-03}.

\begin{lemma}[Substitution]
\label{lemma:substitution}
  For every value substitution $\sigma$,
  for every substitution~$\sigma'$,
  where $\sigma'$ is equal to $\sigma \funcomp \cpsv\cdot$,
  \begin{enumerate}
  \item for every value $v$,
        $\subst{\cpsv v}{\sigma'} = \cpsv{\subst v\sigma}$.
  \item for every term $t$ and continuation $c$,
        $\subst{(\cps tc)}{\sigma'} = \cps{\subst t\sigma}{\substc c{\sigma'}}$.
  \end{enumerate}
\end{lemma}

% ------------------------------------------------------------------------------

The third and last fundamental lemma involves ``kubstitutions'', a word that
we coin for substitutions that affect the continuation, but not the term.
%
We wish to express the informal idea that, ``if $\sigma$ does not affect the
term~$t$, then $\sigma$ can be pushed down into $\cps tc$, where it vanishes
on the term side, and remains $\sigma$ on the continuation side''.
%
This is an interesting statement, whose standard (informal, pencil-and-paper)
formulation is simple, yet whose formulation in de Bruijn style requires a
little thought.
%
Indeed, the condition that ``$\sigma$ does not affect the term~$t$'' should
\emph{not} be expressed by the equation $\subst t\sigma=t$. That would be too
restrictive. For instance, the substitution $\singleton v$ intuitively ``does
not affect'' the term $\lift 1t$, where the variable~0 does not occur free;
yet, the result of the substitution application
$\subst{(\lift 1t)}{\singleton v}$ is~$t$, which is not equal to $\lift 1t$.
%
In other words, a substitution that ``does not affect'' a term can still cause
its free variables to be renumbered.

A more general approach is to allow the term~$t$ to carry an end-of-scope
mark, which is represented by an injective renaming~$\theta$.
(We have already encountered several renamings that can be interpreted
as end-of-scope marks, such as $\liftop1$, $\liftop2$, and $\upplus$.)
One then requires the substitution~$\sigma$ to act only upon the variables
which $\theta$ causes to go out of scope: this is very elegantly expressed
by the equation $\theta\scomp\sigma=\ids$.
%
(This equation implies that $\theta$ is an injective renaming.)
%
For instance, instantiating $\theta$ with $\liftop1$
and $\sigma$ with $\singleton v$ satisfies this equation.

We thus obtain the following statement.
In short, if the composition $\theta;\sigma$ vanishes, then $\sigma$ can be
pushed down into $\cps{\subst t\theta}c$, where it annihilates with $\theta$
on the term side, and remains $\sigma$ on the continuation side.

\begin{lemma}[Kubstitution]
\label{lemma:kubstitution}
  For all substitutions~$\theta$ and $\sigma$,
  where the composition $\theta\scomp\sigma$ is the identity substitution,
  \begin{enumerate}
  \item for every value $v$,
        $\subst{\cpsv{\subst v\theta}}\sigma = \cpsv v$.
  \item for every term $t$ and continuation $c$,
        $\subst{\cps {(\subst t\theta}c)}\sigma = \cps t{\substc c\sigma}$.
  \end{enumerate}
\end{lemma}

It might seem as if Lemma~\ref{lemma:kubstitution} is a corollary of
Lemma~\ref{lemma:substitution}. In fact, it is not, as it does not require
$\sigma$ to be expressible under the form $\_ \funcomp \cpsv\cdot$.

As a corollary, we get the equality:
%
\[ \subst{(\cps{\lift 1t}c)}{\singleton v} = \cps t{\substc c{\singleton v}} \]
%
This equality is used without justification by Danvy and Filinski
~\citeyear[proof of Lemma~3]{danvy-filinski-92}
and forms the second part of Danvy and Nielsen's
Lemma~1~\citeyear{danvy-nielsen-03}. The statement found there may seem
simpler, but in reality lacks a freshness hypothesis, which corresponds to our
use of $\liftop1$.
% Although Danvy and Nielsen report that ``the equation follows
% directly from the definition'', it seems clear that induction is required.

Using Lemma~\ref{lemma:kubstitution}, we establish a few equations that
clarify how the CPS transformation behaves when applied to a term whose
immediate subterms are values.

\begin{lemma}[Special cases]
  For all values $v_1$, $v_2$, for every term $t_2$, for every continuation~$c$,
  \[\begin{array}{rcl}
    \cps{\App{v_1}{t_2}}c & = &
    \cpsnontail {t_2} {\AppDouble{\lift 1{\cpsv{v_1}}}{\Var 0}{\lift 1{(\reify c)}}}
    \\
    \cps{\App{v_1}{v_2}}c & = &
    \AppDouble{\cpsv{v_1}}{\cpsv{v_2}}{(\reify c)}
    \\
    \cps{\Let{v_1}{t_2}}c & = &
    \Let{\cpsv{v_1}}{\cps{t_2}{\liftc 1c}}
  \end{array}\]
\end{lemma}
