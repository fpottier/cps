\section{Introduction}

% ------------------------------------------------------------------------------

% The origins of CPS.

% Fischer (1993) writes that the first reference to CPS of which he is aware
% is due to van Wijngaarden (1966). In fact, Reynolds' paper (1993) contains
% fascinating excerpts of discussions (at the 1966 conference) which clearly
% show that van Wijngaarden fully understood the CPS transformation and the
% notion of a tail call, ten years before Steele's paper.

The transformation of call-by-value $\lambda$-terms into continuation-passing
style, independently discovered by many researchers \cite{reynolds-93} and
first explicitly formulated by Fischer \citeyear{fischer-72,fischer-93}, was
first proved correct by Plotkin~\citeyear{plotkin-75}.

% ------------------------------------------------------------------------------

% Danvy and Filinski's paper.

Plotkin's CPS transformation produces many ``administrative'' redexes, which
are a source of inefficiency and complicate the proof of correctness of the
transformation. To address this issue,
Danvy and Filinski~\citeyear[Figure~2]{danvy-filinski-92} propose a
``one-pass'' CPS transformation, which
is so called because it produces no administrative redexes and therefore does
not require a second pass during which such redexes are eliminated.
Furthermore, they give a ``properly tail-recursive'' variant of the
transformation~\cite[Figure~3]{danvy-filinski-92}, where care is taken not to
produce a harmful $\eta$-redex when a tail call is translated. Finally, they
establish the correctness of this transformation via a simple
simulation argument. If the source program is able to make one step of
computation, then the transformed program is able to follow suit in one or
more steps~\cite[Lemma~3]{danvy-filinski-92}.

\newcommand{\transformationtime}{trans\-for\-ma\-tion-time\xspace}

Danvy and Filinski's transformation is presented in a ``higher-order'' form,
where some of the transformation functions are parameterized with
``\transformationtime continuations'' which are \transformationtime functions
of terms to terms. Danvy and Nielsen \citeyear{danvy-nielsen-03} later propose
a simpler but less elegant ``first-order'' formulation, where
\transformationtime continuations are just terms. When the source language is
the pure \lc, the two formulations define the same transformation, so the same
simulation property holds~\cite[Lemma~2]{danvy-nielsen-03}. When the source
language is extended with a \kw{let} construct, the two transformations no
longer coincide: Danvy and Nielsen's transformation produces some
administrative redexes which Danvy and Filinski's transformation
eliminates (\sref{sec:dnlet}).

% ------------------------------------------------------------------------------

% This paper.

% Teaching was our initial motivation for investigating the CPS
% transformation.

In this paper, we set out to give a crisp, machine-checked account of Danvy
and Filinski's properly tail-recursive CPS transformation, in the setting of
the pure \lc extended with a \kw{let} construct. Although it may seem as if
every aspect of the CPS transformation has been documented already in the
literature, we encounter a few unexpected difficulties along the way, and make
the following contributions:

% ------------------------------------------------------------------------------

% Contributions.

\begin{enumerate}

\item We make the (retrospectively entirely obvious) observation that a
  \transformationtime continuation can be represented as a context, that is,
  as a term where a distinguished bound variable serves as a named hole. This
  leads us to propose a first-order formulation of the CPS transformation
  that is very close to Danvy and Filinski's higher-order formulation and
  does not exhibit the shortcomings of Danvy and Nielsen's first-order
  formulation.
  % This makes proofs easier, because there is no junk in continuations.
  % There is no need to require that continuations be ``schematic''.

\item We reduce the duplication inherent in Danvy and Filinski's formulation.
  Instead of two transformation functions, which are respectively used in
  ``tail'' and ``nontail'' contexts, we define a single transformation
  function, which receives information about the context as part of its
  continuation argument.

\item We represent variables as de Bruijn indices and show that, given the
  current state of the art, this representation does not pose a significant
  obstacle in the definition of the CPS transformation or in its correctness
  proof. We rely on Autosubst~\cite{autosubst-15} to eliminate boilerplate
  definitions and to automate many low-level proof obligations about
  substitutions. Thus, we improve on some of the prior work, such as Minamide
  and Okuma's~\citeyear{minamide-okuma-03}, who use traditional named
  variables and have to explicitly account for $\alpha$-equivalence and
  freshness, and Dargaye and Leroy's~\citeyear{dargaye-leroy-cps-07}, who use
  de Bruijn indices, but go through a nonstandard intermediate language,
  equipped with two name spaces.
  %
  We emphasize that a ``lifting'' operation---that is, the application of
  an injective renaming---should be read as an ``end-of-scope'' operator,
  which indicates that a certain set of variables go out of scope.
  %
  We present an interesting statement (Lemma~\ref{lemma:kubstitution}) that is
  universally quantified in such a renaming~$\theta$.

\item Much to our surprise, we find that Danvy and Filinski's simulation
  argument~\citeyear[Lemma~3]{danvy-filinski-92} breaks down in the presence
  of \kw{let} constructs. Indeed, although Danvy and
  Filinski~\citeyear[Figure~4]{danvy-filinski-92} extend the CPS
  transformation to deal with \kw{let} constructs, among other features, they
  do not extend its correctness proof. We provide a
  counter-example and repair the proof by proposing a novel, slightly more
  complex simulation diagram, which involves call-by-value parallel reduction.
  The theory of parallel reduction~\cite{takahashi-95,crary-standard-09} is
  used to conclude the proof.

\item Although Danvy and Filinski~\citeyear{danvy-filinski-92} remark
  informally that ``the transformation always terminates (and in essentially
  linear time)'', we find that, when variables are represented as de Bruijn
  indices, it seems nontrivial to formulate the CPS transformation so that it
  runs efficiently. To address this problem, we propose a novel higher-order
  formulation of the transformation, which avoids all ``lifting'' operations
  by relying on ``relocatable'' terms and continuations. We prove it
  equivalent to our earlier formulation, and (informally and experimentally)
  check that its time complexity is $O(n\log n)$.

\end{enumerate}

Our definitions and proofs have been machine-checked using Coq and are
electronically available~\cite{online}.

The paper is laid out as follows. We present the CPS transformation, first in
a traditional pencil-and-paper style (\sref{sec:informal}), then in a formal
style (\sref{sec:formal}), where variables are represented as de Bruijn
indices. We state three fundamental lemmas on the interaction between the
transformation and substitutions (\sref{sec:lemmas}), then prove that the
transformation is semantics-preserving (\sref{sec:correctness}) and discuss
how the presence of \kw{let} constructs breaks Danvy and Filinski's simulation
diagram (\sref{sec:let}). Finally, we present an efficient formulation of the
transformation (\sref{sec:efficient}), which we prove correct, before
reviewing the related work~(\sref{sec:related}) and concluding
(\sref{sec:conclusion}).
