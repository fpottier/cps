\section{How \kw{let} constructs complicate matters}
\label{sec:let}

% ------------------------------------------------------------------------------

\paragraph{Simplifications in the absence of \kw{let}.}

\newcommand{\could}{can\xspace}

In the absence of a \kw{let} construct, the statements and proofs in the
previous section \could be simplified as follows.
%
The second rule in the definition of similarity
(Definition~\ref{def:similarity}) \could be removed.
%
In the first item of Lemma~\ref{lemma:similar:apply:reify}, parallel reduction
$\pcbv$ \could be replaced with at most one step of reduction, $\cbv^?$.
%
In the second item of Lemma~\ref{lemma:similar:apply:reify}, parallel
reduction \could (almost miraculously) be replaced with an equality: indeed,
$\reify{(\Tail{(\reify c)})} = \reify c$ holds.
%
In the statement of Lemma~\ref{lemma:magic}, parallel reduction \could then be
replaced with $\cbv^?$.
% In the App case, the second item of Lemma~\ref{lemma:similar:apply:reify} is
% used to miraculously require zero steps of reduction.
%
Finally, in the statement of Lemma~\ref{lemma:simulation}, the relation
$\pluscbvpcbv$ can be replaced with just $\cbv^+$,
%
yielding the simple simulation diagram found in the papers by
Danvy and Filinski~\citeyear[Lemma~3]{danvy-filinski-92} and
Danvy and Nielsen~\citeyear[Lemma~2]{danvy-nielsen-03}.
%
Parallel reduction is not needed any more, so that correctness
(Lemma~\ref{lemma:correctness}) can be established without appeal to the
theory of parallel reduction.

% ------------------------------------------------------------------------------

\paragraph{With \kw{let}, Danvy and Filinski's transformation violates the simple simulation diagram.}

Does the simulation diagram necessarily become more complex in the presence of
\kw{let}? The answer is positive. In the presence of \kw{let}, the
CPS transformation that we study (\sref{sec:formal}), which coincides with
Danvy and Filinski's transformation, does not satisfy the simple simulation
diagram. Here is an example that demonstrates this.
%
Let $t_1$ stand for the term
$
  \App
    {(\Lam{(\Let{\Var 0}{\Var 0})})}
    {(\Lam{\Var 0})}
$,
%
which in informal syntax would be written:
%
\[
  \App
    {(\Lam{z.\Let{w=z}{w}})}
    {(\Lam{x.x})}
\]
This term reduces to $t_2$, which in de Bruijn's notation is
$
  \Let
    {\Lam{\Var 0}}
    {\Var 0}
$
and in informal syntax would be written:
\[
  \Let
    {w=\Lam{x.x}}
    w
\]
According to the simple simulation diagram,
$\cps{t_1}\init \cbv^+ \cps{t_2}\init$ should hold. Yet, this property is
false: the term $\cps{t_1}\init$ does not reduce (in any number of
call-by-value reduction steps) to the term $\cps{t_2}\init$. (We have checked
this in Coq.) Let us explain why that is the case.
%
The term $\cps{t_1}\init$, in informal notation, is:
\[
  \AppDouble
    {(\Lam{z.\Lam{k.\Let{w=z}{\App{k}{w}}}})}
    {(\Lam{x.\Lam{k.\;\App kx}})}
    {(\Lam{w.w})}
\]
This term reduces in two $\beta_v$ steps to:
\[
  \Let{w=\Lam{x.\Lam{k.\;\App kx}}}{\App{(\Lam{w.w})}{w}}
\]
Unfortunately, the term $\cps{t_2}\init$, in informal notation, is:
\[
  \Let{w=\Lam{x.\Lam{k.\;\App kx}}}{w}
\]
We have a ``near miss''. The last two displayed terms differ by the
contraction of a $\beta_v$-redex, which takes place in the right-hand side of
a \kw{let} construct. Such a contraction is not permitted by the relation
$\cbv$.
% It is permitted, though, by the relation $\pcbv$.

This counter-example is one of two counter-examples of minimal size (not
counting variables, the term~$t_1$ has size~4) and involves only one \kw{let}
construct, so it is arguably the ``simplest'' possible counter-example. It was
found by an exhaustive enumeration procedure, illustrating the
fact that testing can help disprove conjectures.
% Could cite QuickCheck, QuickChick, etc.

% ------------------------------------------------------------------------------

\paragraph{With \kw{let}, Danvy and Nielsen's transformation obeys the simple simulation diagram.}
\label{sec:dnlet}

Danvy and Nielsen's transformation is extended with support for \kw{let}
constructs by Minamide and Okuma \citeyear[\S4.4]{minamide-okuma-03}, who
report that this extended transformation still obeys a simple simulation
diagram: when the source program makes one step, the transformed program
follows suit in zero or more steps (up to $\alpha$-equivalence, which in
Minamide and Okuma's paper is explicit).
%
They formally verify this fact using Isabelle/HOL.

This does not contradict our findings, because Minamide and Okuma's
transformation does not coincide with Danvy and Filinski's transformation,
which we study. Indeed, the former produces administrative redexes which the
latter eliminates.
%
Consider, for instance, the term~$t'_2$ defined as $\App{t_2}{\Var 0}$, where
the term~$t_2$ is as above. In informal notation, the term~$t'_2$ can be written:
\[
\App{( \Let {w=\Lam{x.x}} w )} {y}
\]
%
% dncpsinit _ yields:
%   Let (Lam (Lam (App (Var 0) (Var 1)))) (App (Lam (App2 (Var 0) (Var 2) (Lam (Var 0)))) (Var 0))
%
Minamide and Okuma's transformation, applied to $t'_2$
and to the identity continuation $\Lam{z.\Var{z}}$, yields:
\[
\Let{w=\Lam{x.\Lam{k.\;\App{\Var{k}}{\Var{x}}}}}{\App{(\Lam{w.\;\App{\App{\Var{w}}{\Var{y}}}{(\Lam{z.\Var{z}})}})}{\Var{w}}}
\]
This term exhibits an administrative redex $\App{(\Lam w.\ldots)}{\Var w}$.
%
% cps _ (Tail dninit) yields:
%   Let (Lam (Lam (App (Var 0) (Var 1)))) (App2 (Var 0) (Var 1) (Lam (Var 0)))
%
In contrast, Danvy and Filinski's transformation, applied to $t'_2$
and to the object-level identity continuation $\Tail{\Lam{z.\Var{z}}}$, yields:
\[
\Let{w=\Lam{x.\Lam{k.\;\App{\Var{k}}{\Var{x}}}}}{\App{\App{\Var{w}}{\Var{y}}}{(\Lam{z.\Var{z}})}}
\]
This is no surprise: Danvy and Nielsen~\citeyear[\S7.2]{danvy-nielsen-03} note
that their formulation of the CPS transformation cannot be extended with
support for \kw{let} in such a way that it ``flattens nested blocks''. They
write: ``We do not see how a first-order one-pass CPS transformation can
flatten nested blocks in general if it is also to be compositional.'' We show
that this is in fact possible: our formulation (\sref{sec:formal}) is
first-order, compositional, and coincides with Danvy and Filinski's
transformation.
%
The downside of our formulation is that it is not efficient: we now turn to
this issue.
