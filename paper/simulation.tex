\section{Correctness of the CPS transformation}
\label{sec:correctness}

In an untyped setting, a term must exhibit one of three behaviors: it either
converges (that is, reduces in zero, one, or more steps to a value), or
diverges (that is, admits an infinite reduction sequence), or reduces to a
stuck term (a term that cannot reduce, yet is not a value).
%
% (We actually do not formally prove that one of these three must be the case.)
%
% In fact, in the core calculus studied in this paper, a stuck term must be of
% the form $E[\App xv]$, where $E$ is an evaluation context. Thus, there is no
% closed stuck term.
%
We now prove that the CPS transformation is correct: that is, if a source
term~$t$ exhibits one of these behaviors, then the transformed term
$\cps t\init$ exhibits the same behavior, where $\init$ is the empty-context
continuation, $\idc$.

We write $\cbv$ for the (small-step) call-by-value reduction relation, whose
well-known definition we omit. We write $\pcbv$ for the parallel call-by-value
reduction relation, whose definition we also omit: the reader is referred to
Crary's work~\citeyear{crary-standard-09}. Parallel reduction strictly
contains reduction: $(\cbv)\subset(\pcbv)$. In short, parallel reduction
differs from reduction in that (1)~it can contract several redexes at once and
(2)~it can contract redexes under an arbitrary context, including under a
$\lambda$-abstraction and in the right-hand side of a \kw{let} construct.

The lemmas in this section are simple and have relatively short proofs (the
proof scripts for all lemmas, together, take up about 60 nonblank, noncomment
lines). They may seem quite a bit more involved than Danvy and Filinski's
simulation statement~\citeyear[Lemma~3]{danvy-filinski-92}, also found in
Danvy and Nielsen's paper~\citeyear[Lemma~2]{danvy-nielsen-03}.
% Danvy and Nielsen's proof is just ``by induction'',
% but Danvy and Filinski give a detailed proof.
Indeed, the presence of the \kw{let} construct (which the papers just cited do
not handle) complicates matters, requiring us to define a notion of similarity
between continuations, to prove several preliminary lemmas, and to establish a
simulation statement that involves parallel reduction
(Lemma~\ref{lemma:simulation}). In the next section (\sref{sec:let}), we
justify more precisely why this added complexity seems unavoidable.

% I have not defined the notation \cbv^\infty, but I supose it is obvious.

\begin{definition}[Similarity of continuations]
\label{def:similarity}
  The assertion ``$c_1$ is \define{similar} to $c_2$'' is inductively
  defined by the following two rules:
  % A ``similarity'' predicate, which relates two continuations, is defined as follows.
  \begin{enumerate}
  \item $\Tail{(\reify c)}$ is similar to $c$.
  \item If $\metakont_1\pcbv\metakont_2$ holds,
        then $\NonTail{\metakont_1}$ is similar to $\NonTail{\metakont_2}$.
  \end{enumerate}
\end{definition}

% Attempt to explain why similarity must be defined this way:
The first rule in Definition~\ref{def:similarity} must be present because,
in the proof of Lemma~\ref{lemma:simulation},
in the case of $\beta_v$-reduction,
we wish to apply Lemma~\ref{lemma:magic},
instantiated with $\Tail{(\reify c)}$ and $c$.
%
The second rule in Definition~\ref{def:similarity} must be present because,
in the proof of Lemma~\ref{lemma:magic},
in the case where $t$ is a \kw{let} construct,
we wish to apply the induction hypothesis,
instantiated with two continuations $\NonTail{\metakont_1}$ and
$\NonTail{\metakont_2}$ which we can prove (using the induction
hypothesis, again) satisfy $\metakont_1 \pcbv \metakont_2$.

The following lemma is easily established by case analysis. The proof of its
second item relies on the fact that parallel reduction under a
$\lambda$-abstraction is permitted.

\begin{lemma}[Application and reification of similar continuations]
\label{lemma:similar:apply:reify}
  For all continuations~$c_1$ and~$c_2$,
  if $c_1$ is similar to $c_2$, then:
  \begin{enumerate}
  \item for every value~$v$,
        $ \apply{c_1}{\cpsv v} \pcbv \apply{c_2}{\cpsv v} $
        holds;
  \item $ \reify{c_1} \pcbv \reify{c_2} $ holds.
  \end{enumerate}
\end{lemma}

The next lemma is easily established by induction over the size of the
term~$t$. The proof of the case where $t$~is a \kw{let} construct relies on
the fact that parallel reduction in the right-hand side of a \kw{let}
construct is permitted.

\begin{lemma}[Reduction in the continuation]
\label{lemma:magic}
  For all continuations~$c_1$ and~$c_2$,
  if $c_1$ is similar to $c_2$, then,
  for every term~$t$,
  $ \cps t{c_1} \pcbv \cps t{c_2} $
  holds.
\end{lemma}

We then reach the main simulation diagram. This is a forward simulation
diagram, which states that the transformed term is able to simulate every step
of computation taken by the source term. In contrast with the statement proved
by Danvy and Filinski~\citeyear[Lemma~3]{danvy-filinski-92}
and Danvy and Nielsen~\citeyear[Lemma~2]{danvy-nielsen-03},
we must allow the transformed
term to take not just one or more reduction steps $\cbv^+$, but also one
parallel reduction step $\pcbv$. As parallel reduction is a reflexive
relation, this last step can be trivial.

\begin{lemma}[Simulation]
\label{lemma:simulation}
  For all terms~$t_1$ and~$t_2$,
  for every continuation~$c$, provided $\reify c$ is a value,
  $t_1 \cbv t_2$
  implies
  $\cps{t_1} c \pluscbvpcbv \cps{t_2}c$.
\end{lemma}

From this result, there follows that the CPS transformation (initiated with
the identity continuation) is correct: that is, it preserves convergence to a
value, divergence, and ``going wrong'', that is, reducing to a stuck term.

\begin{lemma}[Correctness]
\label{lemma:correctness}
  For every term $t$,
  \begin{enumerate}
  \item if $t \cbv^\star v$ holds, where $v$ is a value, \\
        then there exists a value $v'$ such that
        $\cps t\init \cbv^\star v'$ and $v' \pcbv^\star \cpsv v$.
  \item if $t \cbv^\infty$ holds, \\
        then $\cps t\init \cbv^\infty$ holds as well.
  \item if $t \cbv^\star t'$ holds, where $t'$ is stuck, \\
        then $\cps t\init \cbv^\star t''$,
        where $t''$ is stuck.
  \end{enumerate}
\end{lemma}

The proof of the second item above relies on the fact that reduction and
parallel reduction commute, that is, $\pcbv^\star \;\cdot\; \cbv^+$ is a
subset of $\cbv^+ \;\cdot\; \pcbv^\star$. This fact is proved by
Crary~\citeyear{crary-standard-09}, based on Takahashi's results for
call-by-name \lc~\cite{takahashi-95}. We port Crary's results to Coq, so as to
offer a self-contained proof of our claims.
%
The proofs of the first and third item also exploit the fact that reduction
and parallel reduction commute, but require a more precise statement of this
fact, namely, Crary's Bifurcation lemma~\citeyear[Lemma~9]{crary-standard-09}.

It is easy to prove that the behavior of a CPS-transformed term is the same
under call-by-value and call-by-name evaluation. Indeed, to establish this
result, it suffices to remark that, in such a term, the right-hand side of
every application must be a value, and the left-hand side of every \kw{let}
construct must be a value. This property (which is preserved by reduction)
is sufficient to guarantee indifference.

\begin{lemma}[Indifference]
  For every term~$t$, the term $\cps t\init$ exhibits the same reduction
  sequence under call-by-value and call-by-name reduction semantics.
\end{lemma}

\endinput

% ------------------------------------------------------------------------------

% Notes on the simulation lemma and the lemmas that lead up to it.

1. We need SimilarContext (which relates two NonTail continuations) because in
the proof of pcbv_cps (cases App and Let) the induction hypothesis is applied
to two NonTail continuations. (And these continuations are not equal; if they
were, we could get away without using the induction hypothesis.)

2. In the inductive proof of pcbv_cps, the induction hypothesis is used to
establish that certains continuations are similar. Therefore, there must be an
inclusion between "pcbv" (in the conclusion of pcbv_cps) and "pcbv" (in the
type of SimilarContext).

3. In the proof of pcbv_apply, we require reflexivity, beta-v reduction, and
use the conclusion of SimilarContext. Therefore, three inclusion edges.

4. In the proof of pcbv_reify, we require reflexivity and use the conclusion
of SimilarContext *under a lambda*. Therefore, two inclusion edges.

5. In the proof of pcbv_cps, we use pcbv_apply, and we use pcbv_reify *under
the right-hand side of an application* (whose left-hand side is a value). We
also use the induction hypothesis *under the right-hand side of a Let* (whose
left-hand side is a value).

6. The simulation lemma uses pcbv_cps (whence an inclusion edge) and forces
the presence of SimilarReify. (Which itself is the reason why pcbv_apply
requires beta-v reduction.)

So:

* The four points (SimilarContext, pcbv_apply, pcbv_reify, pcbv_cps) are
equipped with edges that form a strongly connected graph. The same reduction
relation must be used everywhere. It must include zero or one step of beta-v
reduction, and must be closed under the contexts: Lam [], App v [], Let v [].
Parallel reduction is not required.
