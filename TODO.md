* Several notions in [coq/Relations.v](coq/Relations.v) could have more general types.

* Further clean up [coq/AutosubstExtra.v](coq/AutosubstExtra.v).
