This project is a companion to the paper
[Revisiting the CPS Transformation and its Implementation](http://gallium.inria.fr/~fpottier/publis/fpottier-cps.pdf)
by François Pottier.

This project contains:
* A definition of the call-by-value CPS transformation for
  the untyped lambda-calculus (extended with a `let` construct).
  The transformation coincides with [Danvy and Filinski's transformation](https://doi.org/10.1017/S0960129500001535),
  but is presented in a slightly different style.
* A proof that the transformation is semantics-preserving.
* A definition of an efficient implementation of the transformation.
* A proof that the two transformations are equivalent.

The project requires Coq 8.5 and Autosubst.

For an overview of the contents of each file,
please see
[coq/README.md](coq/README.md)
and
[ocaml/README.md](ocaml/README.md).

To compile, please follow these steps:
* Get __[Coq 8.5](https://coq.inria.fr/)__.
  If you already have [opam](https://opam.ocaml.org/doc/Install.html),
  the OCaml package manager,
  then Coq can be installed as follows:
```
  opam repo add coq-released https://coq.inria.fr/opam/released
  opam update
  opam install -j4 -v coq.8.5.3
```
* Install __[Autosubst](https://www.ps.uni-saarland.de/autosubst/)__.
```
  git clone git@github.com:fpottier/autosubst.git
  cd autosubst && make && make install
```
* Then, at the root of the `cps` repository, type:
```
  make
```
