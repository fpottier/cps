.PHONY: all clean

# Compile the Coq code first, as some of the OCaml code is extracted from it.
all:
	$(MAKE) $(MAKEFLAGS) -C coq
	$(MAKE) $(MAKEFLAGS) -C ocaml

clean:
	$(MAKE) $(MAKEFLAGS) -C coq clean
	$(MAKE) $(MAKEFLAGS) -C ocaml clean
