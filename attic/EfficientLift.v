(* An "efficient" (linear-time) [lift] function on terms. *)

Fixpoint efflift i k t :=
  match t with
  | Var x =>
      Var (if lt_dec x k then x else i + x)
  | Lam t =>
      Lam (efflift i (1 + k) t)
  | App t1 t2 =>
      App (efflift i k t1) (efflift i k t2)
  | Let t1 t2 =>
      Let (efflift i k t1) (efflift i (1 + k) t2)
  end.

Lemma foo:
  forall k x sigma,
  x < k ->
  upn k sigma x = Var x.
Proof.
  induction k; intros.
  { false. omega. }
  { destruct x; asimpl.
    { reflexivity. }
    { rewrite IHk by omega. autosubst. } }
Qed.

Lemma bar:
  forall k x i,
  k <= x ->
  upn k (ren (+i)) x = Var (i + x).
Proof.
  induction k; intros.
  { asimpl. reflexivity. }
  { destruct x; [ omega |]. asimpl. rewrite IHk by omega. autosubst. }
Qed.

Lemma foobar:
  forall k x i,
  upn k (ren (+i)) x = Var (if lt_dec x k then x else i + x).
Proof.
  intros. lt_dec.
  { eapply foo. eauto. }
  { eapply bar. omega. }
Qed.

Lemma efflift_correct:
  forall t i k,
  efflift i k t = t.[upn k (ren (+i))].
Proof.
  induction t; intros; asimpl;
  repeat match goal with IH: forall i k, _ |- _ => rewrite IH; clear IH end;
  try reflexivity.
  (* Var *)
  rewrite foobar. reflexivity.
Qed.

Lemma efflift_correct_simplified:
  forall t i,
  efflift i 0 t = lift i t.
Proof.
  intros. eapply efflift_correct.
Qed.
