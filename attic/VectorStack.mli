(* This is a minimal implementation of an imperative stack, based on a
   vector (a dynamically-resizable array). *)

(* Every operation has worst-case time complexity O(1). In the case of
   [push], this is an amortized bound. *)

type 'a stack

(* [empty x] creates a new empty stack. The dummy element [x] is used
   to fill empty space in the underlying array. *)

val empty: 'a -> 'a stack

(* [get s i] returns the stack element currently found in the stack [s]
   at offset [i]. The stack is read from left to right, from oldest to
   newest. Thus, the bottom (oldest) stack element has offset 0, while
   the top (newest) stack element has offset [size s - 1]. *)

val get: 'a stack -> int -> 'a

(* [size s] returns the current size of the stack [s]. *)

val size: 'a stack -> int

(* [push s x] pushes the element [x] onto the end of the stack [s]. *)

val push: 'a stack -> 'a -> unit

(* [truncate s n] truncates the stack [s] so that it contains only [n]
   elements. The integer [n] must be less than or equal to [size
   s]. *)

val truncate: 'a stack -> int -> unit
