(* The following recursion principle allows defining a function of type
   [forall t : term, P t] by induction on the size of its argument. *)

Lemma size_rec:
  forall P : term -> Type,
  forall body : (forall t, (forall u, size u < size t -> P u) -> P t),
  forall t, P t.
Proof.
  intros.
  cut (forall n t, size t < n -> P t). { eauto. }
  intros n. induction n; intros.
  { abstract (omega). }
  { apply body. intros. apply IHn. abstract (omega). }
Defined.

(* This definition of CPS uses [size_rec]. *)

Definition cps : term -> term -> term.
Proof.
  eapply (size_rec (fun _ => term -> term)).
  intros t cps k.
  destruct t as [ x | t | t1 t2 ].
  { refine (App k (Var x)). }
  { refine (App k (Lam (* x *) (Lam (* k *) (@cps (shift 1 t) _ (Var 0))))).
    abstract (rewrite size_renaming; simpl; omega). }
  { refine (
      @cps t1 _ (Lam (* x1 *) (
        @cps (shift 1 t2) _ (Lam (* x2 *) (
          App (App (Var 1) (Var 0)) (shift 2 k)
        ))
      ))
    ).
    abstract (simpl; omega).
    abstract (rewrite size_renaming; simpl; omega). }
Defined.
