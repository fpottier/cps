  (* TEMPORARY as in D&N 7.2, but they have a typo (E should be E' on the first line,
     otherwise the definition does not make sense).
     If we fix the typo, we get this,
     where a [Tail] context is used for [e1], even though it is not in tail position.
     Weird. *)
  { refine (
      cps_ t1 _ (Tail (Lam (* x *) (
        cps_ t2 _ (shiftc 1 c)
      )))
    );
    abstract size.
  }


  (* This version causes the magic step lemma to go through, I think,
     but is not satisfactory, as [t2] is not properly recognized as
     being in tail position. *)
  { refine (
      cps_ t1 _ (NonTail (fun i1 v1 =>
        Let v1 (
          cps_ t2.[ren (upren (+i1))] _ (Tail (shift (1 + i1) (reify c)))
        )
      ))
    );
    abstract size.
  }

Lemma cps_let:
  forall t1 t2 c,
  cps (Let t1 t2) c =
    cps t1 (Tail (Lam (
      cps t2 (shiftc 1 c)
    ))).


    cps t1 (NonTail (fun i1 v1 =>
      Let v1 (
        cps t2.[ren (upren (+i1))] (Tail (shift (1 + i1) (reify c)))
      )
    )).
