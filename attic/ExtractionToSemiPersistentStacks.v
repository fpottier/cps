Require Import EfficientRenamings.

(* Another possible implementation, based on a semi-persistent stack, has
   constant time complexity. This implementation is more "dangerous", as
   my implementation of semi-persistent stacks has not been proved correct.
   Also, it could be the case that the CPS transformation does not obey the
   discipline of semi-persistence; if that is the case, a dynamic check in
   [SemiPersistentStack.validate] should fail. *)

Extract Constant map   => "int SemiPersistentStack.stack".
Extract Constant empty => "(fun () -> SemiPersistentStack.empty 0)".
Extract Constant add   => "(fun i y s -> assert (i = SemiPersistentStack.size s);
                                         SemiPersistentStack.push s y)".
Extract Constant find  => "(fun i s -> SemiPersistentStack.get s i)".
