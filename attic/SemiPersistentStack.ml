(* In a tree of instances, each instance is represented by an individual
   immutable record of type ['a instance], and all instances point to a
   common mutable record of type ['a state], which holds shared state. The
   type ['a stack] that is exposed is a synonym for ['a instance]. *)

(* If this code is compiled with assertion checking turned on, a violation by
   the user of the semi-persistence discipline is detected; see [validate]. If
   compiled with assertion checking turned off, then a violation is not
   detected; this is unsafe. *)

(* Shared state. *)

type 'a state = {
  (* The current version number is the version number of the most recently
     created instance. *)
  mutable current: int;
  (* The data stack contains the data elements for all valid instances
     simultaneously. *)
  data: 'a VectorStack.stack;
  (* The version stack contains the version numbers of all valid instances
     except the root. We exploit the fact that, at every time, the sizes of
     the valid instances form an interval from [0] to [n], for some integer
     [n], and for each integer [i] in this interval, there exists exactly
     one valid instance whose size is [i]. If [i] is nonzero, we store the
     version number of this instance at offset [i-1] in the version stack.
     This information can be maintained in constant time and allows us to
     implement [validate] in constant time. *)
  version: int VectorStack.stack;
  (* At any time, the data stack and the version stack have the same size. *)
}

(* Per-instance state. *)

type 'a instance = {
  (* A pointer to the shared state. *)
  state: 'a state;
  (* The version number of this instance. *)
  number: int;
  (* The stack size of this instance. *)
  size: int;
}

(* [empty] allocates both a fresh [state] record and a fresh instance,
   which becomes the root of a new instance tree. *)

let empty (dummy : 'a) : 'a instance =
  (* Initialize the shared state. *)
  let state = {
    current = 0;
    data = VectorStack.empty dummy;
    version = VectorStack.empty 0;
  } in
  (* Create an instance. *)
  let size = 0 in
  let number = state.current in
  { state; number; size }

(* At any time, the valid instances are: 1- the root instance, whose [size]
   field is zero; and 2- the instances whose [size] field is less than or
   equal to the size of the data & version stacks and whose version number
   is stored, at offset [size - 1], in the version stack. *)

let validate { state; number; size } : unit =
  assert (size <= VectorStack.size state.version);
  assert (size = 0 || VectorStack.get state.version (size - 1) = number)

(* In [get], we check that the index [i] is within bounds for this instance.
   [instance.size] can be less than [VectorStack.size instance.state.data],
   so the dynamic check performed by [VectorStack.get] is not sufficient. *)

let get (instance : 'a instance) (i : int) : 'a =
  assert (0 <= i && i < instance.size);
  validate instance;
  VectorStack.get instance.state.data i

(* For efficiency, [size] does not call [validate], although it could. *)

let size (instance : 'a instance) : int =
  instance.size

let push (instance : 'a instance) (x : 'a) : 'a instance =
  validate instance;
  let { state; number; size } = instance in
  (* Allocate a new version number. *)
  let number = state.current + 1 in
  (* Update the three components of the shared state, as follows. *)
  (* 1. Truncate the data stack at [size], then push [x] onto it. *)
  VectorStack.truncate state.data size;
  VectorStack.push state.data x;
  assert (
    (* 2. Increment the current version number: *)
    state.current <- number;
    (* 3. Truncate the version stack at [size], then push the current version
       number onto it. *)
    VectorStack.truncate state.version size;
    VectorStack.push state.version number;
    true
  );
  (* Allocate a new instance. *)
  let size = size + 1 in
  { state; number; size }

type 'a stack =
  'a instance
