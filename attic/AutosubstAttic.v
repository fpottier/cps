Lemma shift_zero:
  forall t,
  shift 0 t = t.
Proof.
  autosubst.
Qed.

Lemma shift_shift_exchange:
  forall i j t,
  (shift i t).[upn i (ren(+j))] = shift (j + i) t.
Proof.
  intros.
  rewrite shift_upn by tc.
  erewrite shift_shift by tc.
  rewrite Nat.add_comm.
  reflexivity.
Qed.

Lemma plus_iterate_upren:
  forall i xi,
  (+i) >>> iterate upren i xi = xi >>> (+i).
Proof.
  induction i; intros.
  { rewrite iterate_0. autosubst. }
  { rewrite iterate_S.
    replace (+S i) with ((+i) >>> S) by autosubst.
    rewrite compA. rewrite <- IHi. autosubst. }
Qed.
