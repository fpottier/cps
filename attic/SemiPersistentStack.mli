(* This is a minimal implementation of a semi-persistent stack, based on an
   imperative stack. *)

(* In a persistent data structure, an update operation creates a new instance
   of the data structure, without altering existing instances. Thus, at every
   time, there exists a tree of instances, where there is an edge from [x] to
   [y] if [y] was obtained from [x] via an update. All instances in the tree
   are simultaneously valid.

   In a semi-persistent data structure, an update operation also creates a new
   instance [y] out of an existing instance [x], but *invalidates* every
   existing instance that is not an ancestor of [x]. Thus, at every time, the
   valid instances form one branch in the tree, whose tip is the most recent
   instance. Any attempt to use an invalid instance is an error, and is
   detected at runtime. *)

(* This semi-persistent stack can be viewed as a layer of dynamic checks on
   top of an imperative stack. The dynamic checks ensure that the discipline
   of semi-persistence is respected by the user, that is, the user never
   attempts to use an instance that has been invalidated.

   Provided that this discipline is respected, a semi-persistent data
   structure behaves like a persistent data structure. As far as the client is
   concerned, the function call [push s x] produces a new stack without
   affecting the stack [s], which remains valid. *)

(* Every operation has worst-case time complexity O(1). In the case of [push],
   this is an amortized bound. *)

type 'a stack

(* [empty x] creates a new empty stack. The dummy element [x] is used to fill
   empty space in the underlying array. The newly created stack can be viewed
   as the root of a new tree of stacks, where every [push] operation gives
   rise to an edge. The empty stack always remains valid. *)

val empty: 'a -> 'a stack

(* [get s i] returns the stack element found in the stack [s] at offset [i].
   The stack is read from left to right, from oldest to newest. Thus, the
   bottom (oldest) stack element has offset 0, while the top (newest) stack
   element has offset [size s - 1]. The stack [s] must be valid; otherwise,
   this operation fails. *)

val get: 'a stack -> int -> 'a

(* [size s] is the size of the stack [s]. *)

val size: 'a stack -> int

(* [push s x] pushes the element [x] onto the end of the stack [s], yielding a
   new stack [s']. The stack [s] must be valid; otherwise, this operation
   fails. The stack [s] remains valid and is unmodified. Every existing stack
   is invalidated unless it is an ancestor of [s] in the tree. *)

val push: 'a stack -> 'a -> 'a stack

(* [validate s] checks that the stack [s] is valid. If [s] is invalid, this
   operation fails. It can be used for debugging. *)

val validate: 'a stack -> unit
