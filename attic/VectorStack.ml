type 'a stack = {
  (* A dummy element, used to fill empty slots. *)
  dummy: 'a;
  (* The logical size of the stack. *)
  mutable size: int;
  (* The data array. *)
  mutable data: 'a array;
}

let empty (dummy : 'a) : 'a stack =
  let size = 0 in
  let capacity = 64 in
  let data = Array.make capacity dummy in
  { dummy; size; data }

let get (stack : 'a stack) (i : int) : 'a =
  assert (0 <= i && i < stack.size);
  stack.data.(i)

let size (stack : 'a stack) : int =
  stack.size

let grow (stack : 'a stack) : unit =
  assert (stack.size = Array.length stack.data);
  (* The following will fail if [2 * stack.size] exceeds [Sys.max_array_length],
     but that's fine. If it succeeds, then the array has effectively grown by at
     least one slot (and many more). *)
  let data = Array.make (2 * stack.size) stack.dummy in
  Array.blit stack.data 0 data 0 stack.size;
  stack.data <- data

let push (stack : 'a stack) (x : 'a) : unit =
  let i = stack.size in
  (* If the stack is full, grow it. *)
  if i = Array.length stack.data then
    grow stack;
  (* Write the new element and adjust the stack pointer. *)
  stack.data.(i) <- x;
  stack.size <- i + 1

let truncate (stack : 'a stack) (size : int) : unit =
  assert (0 <= size && size <= stack.size);
  stack.size <- size
