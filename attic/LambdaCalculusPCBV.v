Require Import Omega.
Require Import Autosubst.Autosubst.
Require Import AutosubstExtra.
Require Import Sequences.
Require Import LambdaCalculusSyntax.
Require Import LambdaCalculusValues.
Require Import LambdaCalculusReduction.

Lemma pcbv_out_of_Lam:
  forall u1 t2,
  pcbv (Lam u1) t2 ->
  exists u2,
  t2 = Lam u2 /\ pcbv u1 u2.
Proof.
  inversion 1; subst; eauto.
Qed.

Ltac pcbv_out_of_Lam :=
  match goal with h: pcbv (Lam ?u1) ?t2 |- _ =>
    destruct (pcbv_out_of_Lam _ _ h) as (?&?&?); subst t2
  end.

Lemma pcbv_Lam_Lam:
  forall u1 u2,
  pcbv (Lam u1) (Lam u2) ->
  pcbv u1 u2.
Proof.
  inversion 1; subst; eauto.
Qed.

Definition fpcbv : term -> term.
Proof.
  (* The definition is by well-founded recursion on the size of [t]. *)
  refine (Fix smaller_wf_transparent (fun _ => term) _).
  intros t self.
  (* The definition is by cases on the term [t]. *)
  destruct t as [ x | t | t1 t2 | t1 t2 ].
  (* [Var x] *)
  { refine (Var x). }
  (* [Lam t] *)
  { refine (Lam (self t _)); abstract size. }
  (* [App t1 t2] *)
  { case_eq t1; intros.
    { refine (App (self t1 _) (self t2 _)); abstract size. }
    { refine (if_value t2 _ _).
      { match goal with h: t1 = Lam ?body1 |- _ =>
          refine ((self body1 _).[self t2 _/])
        end; subst; abstract size. }
      { refine (App (self t1 _) (self t2 _)); abstract size. }
    }
    { refine (App (self t1 _) (self t2 _)); abstract size. }
    { refine (App (self t1 _) (self t2 _)); abstract size. }
  }
  (* [Let t1 t2] *)
  { refine (if_value t1 _ _).
    { refine ((self t2 _).[self t1 _/]); abstract size. }
    { refine (Let (self t1 _) (self t2 _)); abstract size. }
  }
Defined.

Require Import FixExtra.

Lemma fpcbv_Var:
  forall x,
  fpcbv (Var x) = Var x.
Proof.
  reflexivity.
Qed.

Lemma fpcbv_Lam:
  forall t,
  fpcbv (Lam t) = Lam (fpcbv t).
Proof.
  intros. erewrite Fix_eq_simplified with (f := fpcbv) by reflexivity.
  reflexivity.
Qed.

Lemma fpcbv_App_1:
  forall t1 t2,
  ~ is_value t2 ->
  fpcbv (App t1 t2) = App (fpcbv t1) (fpcbv t2).
Proof.
  intros. erewrite Fix_eq_simplified with (f := fpcbv) by reflexivity. simpl.
  destruct t1; try reflexivity. if_value. reflexivity.
Qed.

Lemma fpcbv_App_2:
  forall t1 t2,
  (forall u1, t1 <> Lam u1) ->
  fpcbv (App t1 t2) = App (fpcbv t1) (fpcbv t2).
Proof.
  intros. erewrite Fix_eq_simplified with (f := fpcbv) by reflexivity. simpl.
  destruct t1; try reflexivity. false. generalize (H t); intro. congruence.
Qed.

Lemma fpcbv_BetaV:
  forall t1 t2,
  is_value t2 ->
  fpcbv (App (Lam t1) t2) = (fpcbv t1).[fpcbv t2/].
Proof.
  intros. erewrite Fix_eq_simplified with (f := fpcbv) by reflexivity. simpl.
  if_value. reflexivity.
Qed.

Lemma fpcbv_Let:
  forall t1 t2,
  ~ is_value t1 ->
  fpcbv (Let t1 t2) = Let (fpcbv t1) (fpcbv t2).
Proof.
  intros. erewrite Fix_eq_simplified with (f := fpcbv) by reflexivity. simpl.
  if_value. reflexivity.
Qed.

Lemma fpcbv_LetV:
  forall t1 t2,
  is_value t1 ->
  fpcbv (Let t1 t2) = (fpcbv t2).[fpcbv t1/].
Proof.
  intros. erewrite Fix_eq_simplified with (f := fpcbv) by reflexivity. simpl.
  if_value. reflexivity.
Qed.

Global Opaque fpcbv.

Ltac fpcbv :=
  first [
    rewrite fpcbv_Var
  | rewrite fpcbv_Lam
  | rewrite fpcbv_App_1 by obvious
  | rewrite fpcbv_App_2 by congruence
  | rewrite fpcbv_BetaV by obvious
  | rewrite fpcbv_Let by obvious
  | rewrite fpcbv_LetV by obvious
  ].



Ltac one_step :=
  eapply star_step; [ obvious | eapply star_refl_eq; eauto ].

Definition composition {A} (R1 R2 : A -> A -> Prop) (x z : A) :=
  exists y, R1 x y /\ R2 y z.

Definition inclusion {A} (R1 R2 : A -> A -> Prop) :=
  forall x y, R1 x y -> R2 x y.

(* TEMPORARY now false:

(* If [t1] reduces to [Lam u2] via parallel call-by-value reduction, then
   either [t1] is already a lambda-abstraction, or this reduction step is in
   fact allowed by the [indifferent] strategy. *)

Lemma pcbv_into_Lam:
  forall t1 u2,
  pcbv t1 (Lam u2) ->
  (exists u1, t1 = Lam u1 /\ pcbv u1 u2) \/ indifferent t1 (Lam u2).
Proof.
  inversion 1; obvious. (* gotta love this proof *)
Qed.

Ltac pcbv_into_Lam :=
  match goal with h: pcbv ?t1 (Lam _) |- _ =>
    destruct (pcbv_into_Lam _ _ h) as [ (?&?&?) |]; [subst t1 |]
  end.

(* If [t1] reduces to a value [v2] via parallel call-by-value reduction, then
   either [t1] is already a value, or this reduction step is in fact allowed
   by the [indifferent] strategy. *)

Lemma pcbv_into_value:
  forall t1 v2,
  pcbv t1 v2 ->
  is_value v2 ->
  is_value t1 \/ indifferent t1 v2.
Proof.
  inversion 1; obvious. (* gotta love this proof *)
Qed.

Ltac pcbv_into_value :=
  match goal with h1: pcbv ?u1 ?u2, h2: is_value ?u2 |- _ =>
    destruct (pcbv_into_value _ _ h1 h2)
  end.

*)

Lemma pcbv_indiff_preliminary:
  forall t1 t2,
  pcbv t1 t2 ->
  forall u2,
  indifferent t2 u2 ->
  exists u1,
  star indifferent t1 u1 /\
  pcbv u1 u2.
Proof.
  induction 1; try solve [ tauto ]; intros ? Hi; subst.
  (* By cases on the vertical [pcbv] step. *)

  (* RedParBetaV *)
  { eexists. split. step. one_step. obvious.

  (* RedParLetV *)
  { eexists. split. obvious. one_step. eauto using indifferent_subset_pcbv. }

  (* RedLam *)
  { false. inversion Hi; tauto. }

  (* RedAppLR *)
  { inversion Hi; clear Hi; try solve [ tauto ]; subst. clear IHred2.
    (* By subcases on the horizontal [indifferent] step. *)

    (* Subcase: [RedBetaV]. *)
    { clear IHred1. pcbv_into_Lam.
      (* Subsubcase: [t1] was already a lambda-abstraction. *)
      { pcbv_into_value.
        { eexists. split. obvious. eapply pcbv_parallel_subst; obvious. }
        { admit.
          (* PROBLEM: [indifferent] does not allow reducing [u1] to [u2]
             in the right-hand side of an application. But, in the image
             of the CPS translation, the right-hand side of every application
             is a value. *)
        }
      }
      (* Subsubcase: [t1] was not already a lambda-abstraction, so its
         reduction towards [t2] is in fact a computational step that
         the [indifferent] strategy allows. *)
      { assert (is_value u1). { admit. }
        eexists. split. obvious. step. one_step. }
    }

    (* Subcase: [RedAppL]. *)
    { destruct (IHred1 t3) as (?&?&?). { eauto. }
      eexists. split. obvious. eauto using star_pcbv_App. }
  }

  (* RedLetLR *)
  { inversion Hi; clear Hi; try solve [ tauto ]; subst.
    (* By subcases on the horizontal [indifferent] step. *)

    (* Subcase: [RedLetV]. *)
    { pcbv_into_value.
      { eexists. split. obvious. one_step. eauto using pcbv_parallel_subst. }
      { eexists. split. obvious. step. one_step. eauto using pcbv_parallel_subst, red_refl. }
    }
    (* Subcase: [RedLetL]. *)
    { destruct (IHred1 t3) as (?&?&?). { eauto. }
      eexists. split. obvious. eauto using star_pcbv_Let. }
  }

  (* RedVar *)
  { false. inversion Hi; tauto. }

Admitted.

Lemma pcbv_indiff:
  inclusion
    (composition pcbv indifferent)
    (composition indifferent (star pcbv)).
Proof.
  unfold inclusion, composition.
  intros t1 u2 [ t2 [ ? ? ]].
  eauto using pcbv_indiff_preliminary.
Qed.
