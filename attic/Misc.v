Definition if_value {A} (t : term) (a1 : A) (a2 : (size t > 0) -> A) : A.
Proof.
  destruct t.
  (* Var *)
  exact a1.
  (* Lam *)
  exact a1.
  (* App. *)
  eapply a2. abstract (simpl; omega).
Defined.

Lemma ren_cpsv:
  forall i,
  ren (+i) >>> cpsv = ids >>> cpsv >> ren (+i).
Proof.
  intros. reflexivity. (* ??? *)
Qed.

(* The following fact appears in the proof of [bind_simulation]. *)

Goal
  forall t K,
  (forall v, (K 1 (Var 0)).[v/] = K 0 v) ->
  star cbv (cps t (Lam (K 1 (Var 0)))) (bind t K).
Proof.
  intros. value_or_nonvalue t; bind.
  (* Case: [t] is a value. *)
  { cps'. step. (* implicitly uses the hypothesis about [K] *)
    finished. reflexivity. }
  (* Case: [t] is not a value. *)
  { finished. reflexivity. }
Qed.

Goal
  forall t k,
  cps t k = bind t (fun i v => App (shift i k) v).
Proof.
  intros. value_or_nonvalue t; bind.
  { cps'. asimpl. reflexivity. }
  { (* This case does not work: we have an eta-expansion. *)
Abort.
