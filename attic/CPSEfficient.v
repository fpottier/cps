(* We could also give a direct definition of [lambda], which does not rely on [var]. *)

Definition lambda' t :=
  fun dst =>
    Lam (
      let dst := 1 + dst in
      let x := fun dst' => Var (dst' - 1 - (dst - 1)) in
      t x dst
    ).

Goal forall t, lambda t = lambda' t.
Proof.
  intros. unfold lambda', lambda, var.
  f_ext; intros dst.
  do 2 f_equal.
  f_ext; intros dst'.
  rewrite <- minus_n_O.
  reflexivity.
Qed.
