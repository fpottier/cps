Require Import ExtrOcamlNatInt. (* Use OCaml native integers. *)
Require Import CPSDefinition.
Require Import EfficientRenamings.
Require Import CPSEfficient.

(* When extracting our efficient implementation of the CPS transformation,
   we must provide OCaml implementations of the type [map] and of its
   operations [empty], [add], [find]. *)

(* The following implementation, based on OCaml's immutable balanced binary
   search trees, has logarithmic time complexity. We do not prove it correct
   in Coq. *)

Extract Constant map   => "int IntMap.t".
Extract Constant empty => "(fun () -> IntMap.empty)".
Extract Constant add   => "IntMap.add".
Extract Constant find  => "IntMap.find". (* no exception should be raised *)

(* The extraction command. *)

Extraction "../ocaml/CPS"
  cpsinit    (* an inefficient implementation of the CPS transformation   *)
  rcpsinit.  (*   an efficient implementation of the CPS transformation   *)
