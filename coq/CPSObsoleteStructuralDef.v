Require Import Coq.Arith.Compare_dec. (* [lt_dec] *)
Require Import MyTactics.
Require Import LambdaCalculusSyntax.
Require Import LambdaCalculusValues.
Require Import LambdaCalculusReduction.
Require Import CPSDefinition.

(* Below, we give an alternate definition of the CPS transformation, which is
   by induction on the structure of terms. To achieve this, we parameterize
   the function [CPS] with a renaming [theta], which is intended to be applied
   to the term [t], not to the continuation [c]. *)

(* Unfortunately, this definition is not easy to come up with, nor is it easy
   to work with, which is why, when proving properties of the CPS transform,
   we prefer to work with the function [cps] defined in CPSDefinition.
   Furthermore, although I initially thought that this definition would lead
   to an efficient implementation, it does not, due to one remaining call to
   [lift] in the treatment of applications. Thus, this file is obsolete, but I
   keep it for the record. An efficient implementation of the CPS
   transformation is given in the file [CPSEfficient]. *)

(* We represent the renaming [theta] as a substitution of type [var -> term]
   as opposed to a renaming of type [var -> var] because that is slightly more
   convenient. Two operations on [theta] are required:

   1. When a binder in the source term is entered, [theta] becomes [up theta],
   which means that both the source and target name spaces are extended with a
   new name, which is mapped to itself.

   2. When a fresh variable is allocated in the target name space, [theta]
   becomes [theta >> ren (+1)], which means that every source name [x] must
   now be mapped to [lift 1 (theta x)]. *)

Fixpoint CPS (theta : var -> term) (t : term) (c : continuation) : term :=
  match t with
  | Var x =>
      apply c (theta x)
  | Lam t =>
      apply c (
        Lam (
          let theta := up theta in
          Lam (
            let theta := theta >> ren (+1) in
            CPS theta t (O (Var 0))
          )
        )
      )
  | App t1 t2 =>
      CPS theta t1 (M (
        let theta := theta >> ren (+1) in
        CPS theta t2 (M (
          App (App (Var 1) (Var 0)) (lift 2 (reify c))
        ))
      ))
  | Let t1 t2 =>
      CPS theta t1 (M (
        let theta := theta >> ren (+1) in
        Let (Var 0) (
          let theta := up theta in
          CPS theta t2 (liftc 2 c)
        )
      ))
  end.

(* The following trivial lemmas are used in the proof of [CPS_cps_general]. *)

Lemma down_into_lam:
  forall theta1 theta2 sigma xi,
  theta1 >> theta2 = sigma ->
  up theta1 >> (up theta2 >> xi) = up sigma >> xi.
Proof.
  intros ? ? ? ? Heq. rewrite scompA, up_comp, Heq. reflexivity.
Qed.

Lemma down_into_app:
  forall theta1 theta2 sigma xi,
  theta1 >> theta2 = sigma ->
  theta1 >> (theta2 >> xi) = sigma >> xi.
Proof.
  intros ? ? ? ? Heq. rewrite scompA, Heq. reflexivity.
Qed.

Lemma down_into_let:
  forall theta1 theta2 sigma xi,
  theta1 >> theta2 = sigma ->
  up theta1 >> up (theta2 >> xi) = up (sigma >> xi).
Proof.
  intros ? ? ? ? Heq. rewrite up_comp, scompA, Heq. reflexivity.
Qed.

Local Ltac down :=
  first [
    eapply down_into_lam; obvious
  | eapply down_into_app; obvious
  | eapply down_into_let; obvious
  | obvious
  ].

(* The next lemma states that the functions [CPS] and [cps] are equivalent.
   It is stated in a general form that is suitable for an inductive proof:
   [CPS theta2 t.[theta1] c] is equal to [cps t.[theta1 >> theta2] c]. In
   other words, the first parameter of [CPS] is conceptually applied to the
   term [t] prior to the CPS transformation. *)

Lemma CPS_cps_general:
  forall t theta1 theta2 sigma c,
  is_ren theta1 ->
  is_ren theta2 ->
  theta1 >> theta2 = sigma ->
  CPS theta2 t.[theta1] c = cps t.[sigma] c.
Proof.
  induction t as [| | t1 IHt1 t2 IHt2 | t1 IHt1 t2 IHt2 ];
  intros ? ? ? ? Htheta1 Htheta2 Heq; simpl.
  (* Var *)
  { destruct Htheta1 as [ xi1 ? ].
    destruct Htheta2 as [ xi2 ? ]. subst.
    simpl. cps. cpsv. reflexivity. }
  (* Lam *)
  { cps. cpsv. asimpl. erewrite IHt by down. reflexivity. }
  (* App *)
  { cps.
    erewrite IHt1 by eauto.
    do 2 f_equal.
    erewrite IHt2 by down.
    asimpl. reflexivity. }
  (* Let *)
  { cps.
    erewrite IHt1 by eauto.
    do 2 f_equal.
    erewrite IHt2 by down.
    asimpl. reflexivity. }
Qed.

(* This corollary gives a simpler statement of the equivalence between the
   functions [CPS ids] and [cps]. *)

Lemma CPS_cps:
  forall t c,
  CPS ids t c = cps t c.
Proof.
  intros.
  replace t with t.[ids] by autosubst.
  eapply CPS_cps_general; obvious.
Qed.
