This directory contains Coq definitions and proofs.

* [MyTactics](MyTactics.v)
  contains a small number of useful custom tactics.

* [FixExtra](FixExtra.v)
  contains a simplified version of the lemma `Fix_eq`, which is defined in
  [Coq.Init.Wf](https://coq.inria.fr/library/Coq.Init.Wf.html).

* [AutosubstExtra](AutosubstExtra.v) and
  [Autosubst_IsRen](Autosubst_IsRen.v)
  contain a few additions to the library
  [Autosubst](https://github.com/fpottier/autosubst).

* [Sequences](Sequences.v)
  proves a few basic facts about reduction sequences.

* [Relations](Relations.v)
  proves a few basic facts about relations and commutative diagrams.

* [LambdaCalculusSyntax](LambdaCalculusSyntax.v)
  defines the syntax of the lambda-calculus
  as well as a few auxiliary notions,
  such as the size of a term.

* [LambdaCalculusFreeVars](LambdaCalculusFreeVars.v)
  defines the predicates `fv` and `closed`.

* [LambdaCalculusValues](LambdaCalculusValues.v)
  defines the predicate `is_value`
  as well as a number of accompnying lemmas and tactics.

* [LambdaCalculusReduction](LambdaCalculusReduction.v)
  defines several notions of small-step reduction,
  including call-by-value reduction, call-by-name reduction,
  and parallel call-by-value reduction,
  as well as a number of accompnying lemmas and tactics.

* [LambdaCalculusParallelReduction](LambdaCalculusParallelReduction.v) and
  [LambdaCalculusStandardization](LambdaCalculusStandardization.v)
  contain a Coq adaptation of the paper
  [A Simple Proof of Call-by-Value Standardization](https://www.cs.cmu.edu/~crary/papers/2009/standard.pdf),
  by Karl Crary.

* [LambdaCalculusPrinter](LambdaCalculusPrinter.v)
  contains a crude lambda-term printer,
  which can help decipher de Bruijn's notation.

* [CPSDefinition](CPSDefinition.v)
  defines the CPS transformation.

* [CPSContextSubstitution](CPSContextSubstitution.v)
  contains a few technical lemmas about the functions `liftc` and `substc`.

* [CPSRenaming](CPSRenaming.v),
  [CPSSubstitution](CPSSubstitution.v)
  and
  [CPSKubstitution](CPSKubstitution.v)
  prove three fundamental lemmas about the CPS transformation.

* [CPSSpecialCases](CPSSpecialCases.v)
  proves a few basic equations
  which clarify how the CPS transformation behaves
  in certain special cases.

* [CPSSimulation](CPSSimulation.v) and
  [CPSCorrectness](CPSCorrectness.v)
  establish the correctness of the CPS transformation.

* [CPSSimulationWithoutLet](CPSSimulationWithoutLet.v) is a simplified copy of
  [CPSSimulation](CPSSimulation.v)
  where we pretend that the `Let` construct does not exist.
  This file is not used in the rest of the proof; it is provided for
  pedagogical purposes.

* [CPSIndifference](CPSIndifference.v)
  proves that the terms produced by the CPS transformation
  behave in the same way under call-by-value and call-by-name
  semantics.

* [CPSCounterExample](CPSCounterExample.v)
  proves that the simple simulation diagram
  stated in Danvy and Filinski's paper
  breaks down in the presence of a `let` construct.

* [CPSDanvyNielsen](CPSDanvyNielsen.v)
  defines Danvy and Nielsen's transformation,
  extended with support for `let` constructs,
  and illustrates how it differs from
  Danvy and Filinski's transformation.

* [EfficientRenamings](EfficientRenamings.v)
  proposes an efficient machine representation
  of renamings, that is, total functions of
  de Bruijn indices to de Bruijn indices.
  It assumes a correct and efficient
  implementation of finite maps (of integers to integers).

* [CPSEfficient](CPSEfficient.v)
  defines an efficient implementation of the CPS transformation,
  and proves that it is equivalent to the simpler formulation
  in [CPSDefinition](CPSDefinition.v).

* [CPSExtraction](CPSExtraction.v)
  instructs Coq to extract OCaml code out of our definitions.
  This code is used by the little programs in the
  [ocaml](../ocaml) subdirectory.
