Require Import MyTactics.
Require Import FixExtra.
Require Import LambdaCalculusSyntax.
Require Import LambdaCalculusValues.
Require Import EfficientRenamings.
Require Import CPSDefinition.
Require Import CPSContextSubstitution.
Require Import CPSRenaming.
Require Import CPSKubstitution.

(* -------------------------------------------------------------------------- *)

(* Arithmetic trivialities. *)

Global Opaque plus minus.

Lemma arith00: forall x, 0 < 1 + x.
Proof. intros. omega. Qed.

Lemma arith01: forall x y, 0 < x -> 0 < x + y.
Proof. intros. omega. Qed.

Lemma arith02: forall x, x = 0 + x.
Proof. intros. omega. Qed.

Local Hint Resolve arith00 arith01 arith02 plus_assoc.

(* -------------------------------------------------------------------------- *)

(* To avoid the use of [lift] entirely, instead of constructing a term [t], we
   often construct a relocatable term, that is, a function [f], which, when
   applied to the height [dst] of the target name space, produces a term. *)

Definition rterm :=
  nat -> term.

(* -------------------------------------------------------------------------- *)

(* The following auxiliary functions construct relocatable terms. *)

(* If [dst] is the current height of the target name space and [i] is a de
   Bruijn index in the target name space, then the relocatable term
   [var dst i] represents the variable [Var i]. *)

Definition var dst i : rterm :=
  (* Convert the de Bruijn index [i] to a de Bruijn level in the
     current scope, which is given by [dst]. *)
  let i := dst - 1 - i in
  fun dst' =>
    (* Convert the de Bruijn level [i] back to a de Bruijn index
       in the current scope, which is given by [dst']. *)
    let i := dst' - 1 - i in
    Var i.
    (* In effect, we have incremented [i] by the difference between [dst'],
       the target name space height at use time, and [dst], the target name
       space height at construction [time]. This difference is always
       nonnegative. *)

(* If [t] is a relocatable function body -- which expects a relocatable
   representation of the function argument, and produces a relocatable
   representation of the function body -- then [lambda t] is a
   relocatable representation of the function. *)

Definition lambda (t : rterm -> rterm) : rterm :=
  fun dst =>
    Lam (
      let dst := 1 + dst in
      let x := var dst 0 in
      t x dst
    ).

(* [app] is [App], lifted to the level of relocatable terms. *)

Definition app (t1 : rterm) (t2 : rterm) : rterm :=
  fun dst =>
    App (t1 dst) (t2 dst).

(* Just for fun and clarity, check [lambda (fun x => x)] expands to a constant
   function of [dst] which always returns the identity term. *)

Goal
  lambda (fun x => x) = (fun dst => Lam (Var 0)).
Proof.
  f_ext; intros dst. unfold lambda, var. do 2 f_equal. omega.
Qed.

(* -------------------------------------------------------------------------- *)

(* The predicate [rtermR dst t t'] means that the relocatable term [t] has
   been created at a time when the height of the target name space was [dst],
   and represents the term [t']. The relocatable term [t] is intended to be
   applied to a height [dst'] that is greater than or equal to [dst]. If
   [dst'] is [dst], then this application should yield [t']. More generally,
   if [dst' - dst] is [i], then the free names of [t'] should be lifted up by
   [i], so this application should yield [lift i t']. *)

Definition rtermR (dst : nat) (t : rterm) (t' : term) :=
  forall dst' i,
  dst' = i + dst ->
  t dst' = lift i t'.

(* This tactic is used when preparing to prove [rtermR _ _ _]. *)

Ltac prove_rtermR :=
  repeat intro; subst.

(* This lemma helps use exploit a fact of the form [rtermR _ _ _] in the
   special case where [i] is zero. *)

Lemma use_rtermR_at_zero:
  forall dst t t',
  rtermR dst t t' ->
  t dst = t'.
Proof.
  unfold rtermR. intros ? ? ? h. erewrite (h dst 0). autosubst. eauto.
Qed.

(* The predicate [rtermR] is compatible with lifting. If [t] represents [t']
   when viewed as constructed-at-time-[dst], then [t] can also be viewed as
   constructed-at-time-[dst'], where [dst'] is greater than or equal to [dst],
   and it then represents [lift i t'], where [i] is the difference between
   [dst'] and [dst]. This is the basic reason why we are able to avoid
   explicit invocations of [lift]. *)

Lemma rtermR_lift:
  forall dst dst' i t t',
  rtermR dst t t' ->
  dst' = i + dst ->
  rtermR dst' t (lift i t').
Proof.
  prove_rtermR.
  erewrite lift_lift by tc.
  pick rtermR ltac:(fun h => erewrite <- h by eauto).
  eauto.
Qed.

(* -------------------------------------------------------------------------- *)

(* The following lemmas establish the correctness of [var], [lambda], and
   [app]. *)

Lemma rtermR_var:
  forall dst i,
  i < dst ->
  rtermR dst (var dst i) (Var i).
Proof.
  prove_rtermR. unfold var.
  asimpl. change ids with Var. f_equal.
  omega.
Qed.

Lemma rtermR_lambda:
  forall dst t u,
  (
    forall x i,
    rtermR (1 + i + dst) x (Var 0) ->
    rtermR (1 + i + dst) (t x) u.[up (ren (+i))]
  ) ->
  rtermR dst (lambda t) (Lam u).
Proof.
  prove_rtermR. unfold lambda. rewrite subst_lam. f_equal.
  rewrite plus_assoc.
  eauto using use_rtermR_at_zero, rtermR_var.
Qed.

Lemma rtermR_app:
  forall t1 t2 t'1 t'2 dst,
  rtermR dst t1 t'1 ->
  rtermR dst t2 t'2 ->
  rtermR dst (app t1 t2) (App t'1 t'2).
Proof.
  prove_rtermR. unfold app. rewrite subst_app.
  f_equal; eauto using use_rtermR_at_zero.
Qed.

(* -------------------------------------------------------------------------- *)

(* To avoid the use of [lift], we also keep track of a renaming that must be
   applied to the source term. As described in [EfficientRenamings], a renaming
   [theta] can be represented as a triple [(src, m, dst)]. The height of the
   target name space, [dst], is already distributed to the relocatable terms
   that we construct, so there remains to keep track of the pair [(src, m)],
   which we also call [env]. *)

Implicit Type env : nat * map.

(* [svar x env] is a a relocatable term that stands for the source
   variable [x]. *)

Definition svar x env : rterm :=
  fun dst =>
    (* The pair [(env, dst)] forms a renaming, which we apply to [x] using
       the function [interpret] defined in [EfficientRenamings]. *)
    Var (interpret (env, dst) x).

(* If [t] has type [envT -> rterm], then [sbind t] has type [envT -> rterm]
   as well. Its effect is to apply [_up] to [env], that is, to introduce a
   variable that exists both in the source and target name spaces. *)

(* I find it difficult to write a clear spec for [sbind], [slambda], and
   [slet]; in fact, in the proof that follows, I find it easier to expand them
   away than to state lemmas about them. *)

Definition sbind t env : rterm :=
  fun dst =>
    let (env, dst) := _up (env, dst) in
    t env dst.

(* [slambda env t] is a relocatable term for a target lambda that corresponds
   to a source lambda. *)

Definition slambda env t : rterm :=
  fun dst => Lam (sbind t env dst).

(* [slet env t1 t2] is a relocatable term for a target let-binding that
   corresponds to a source let-binding. *)

Definition slet env t1 t2 : rterm :=
  fun dst => Let (t1 dst) (sbind t2 env dst).

(* -------------------------------------------------------------------------- *)

(* The following lemma establishes the correctness of [svar]. *)

Lemma rtermR_svar:
  forall x env dst theta,
  renamingR (env, dst) theta ->
  rtermR dst (svar x env) (Var x).[theta].
Proof.
  prove_rtermR. unfold svar.
  fold_renaming.
  change Var with ids at 1.
  erewrite use_renamingR by eauto with renamingR typeclass_instances.
  autosubst.
Qed.

(* As noted above, there are no correctness lemmas for [slambda] and
   [slet]. Although I was able to establish reasonable-looking lemmas
   about them, I later found out that these lemmas were unusable, because
   their hypotheses were not right (they were too strong, probably). *)

(* -------------------------------------------------------------------------- *)

(* On top of relocatable terms, we define relocatable continuations. As in
   [CPSDefinition], a continuation is either [RO] or [RM]. In the [RO] case,
   we naturally store a relocatable term instead of just a term. In the [RM]
   case, instead of representing the continuation as a term-with-a-hole (which
   is very inefficient, as we must later substitute a value for the hole!), we
   represent it as a meta-level function of type [rterm -> rterm]. Thus, our
   efficient formulation of the CPS transformation is higher-order. *)

Inductive rcontinuation :=
  | RO (k : rterm)
  | RM (K : rterm -> rterm).

(* We define analogues of the functions [apply] and [reify]. We do not need an
   analogue of [liftc], because our continuations are relocatable. *)

Definition rapply (c : rcontinuation) (v : rterm) : rterm :=
  match c with
  | RO k =>
      app k v
  | RM K =>
      K v
  end.

Definition rreify (c : rcontinuation) : rterm :=
  match c with
  | RO k =>
      k
  | RM K =>
      lambda K
  end.

(* -------------------------------------------------------------------------- *)

(* We now define what it means for a relocatable continuation [c], created at
   level [dst], to represent a continuation [c']. In the case [RO], this
   boils down to saying that the relocatable term [k] represents the term
   [k']. In the case [RM], we require the meta-level function
   application [K v] to yield the same result as filling the hole of [K'] with
   [v'], provided the values [v] and [v'] are related. We must in fact allow
   [v] and [v'] to be related at [dst'], where [dst'] is greater than or equal
   to [dst]. In that case, the free variables of [K'] are lifted up by [i],
   where [i] is precisely the difference [dst' - dst]. *)

Inductive contR : nat -> rcontinuation -> continuation -> Prop :=
| ContRO:
    forall dst k k',
    rtermR dst k k' ->
    contR dst (RO k) (O k')
| ContRM:
    forall dst K K',
    (
      forall i dst' v v',
      rtermR dst' v v' ->
      dst' = i + dst ->
      rtermR dst' (K v) K'.[v' .: ren (+i)]
    ) ->
    contR dst (RM K) (M K').

(* The predicate [contR] is compatible with lifting. If [c] represents [c']
   when viewed as constructed-at-time-[dst], then [c] can also be viewed as
   constructed-at-time-[dst'], where [dst'] is greater than or equal to [dst],
   and it then represents [liftc i c'], where [i] is the difference between
   [dst'] and [dst]. See the lemma [rtermR_lift] for comparison. *)

Lemma contR_liftc:
  forall dst c c',
  contR dst c c' ->
  forall dst' i,
  dst' = i + dst ->
  contR dst' c (liftc i c').
Proof.
  induction 1; intros; subst; simpl; econstructor.
  { eauto using rtermR_lift. }
  { intros. subst. rewrite plus_assoc in *. asimpl. eauto. }
Qed.

(* The functions [rapply] and [apply] are related. *)

Lemma contR_apply:
  forall dst c v c' v',
  contR dst c c' ->
  rtermR dst v v' ->
  rtermR dst (rapply c v) (apply c' v').
Proof.
  induction 1; intros; unfold rapply, apply; prove_rtermR.
  (* O *)
  { rewrite subst_app. unfold app.
    f_equal; eauto using use_rtermR_at_zero. }
  (* M *)
  { match goal with h: context[K] |- _ => erewrite h by eauto; clear h end.
    reflexivity. }
Qed.

(* The functions [rreify] and [reify] are related. *)

Lemma contR_reify:
  forall dst c c',
  contR dst c c' ->
  rtermR dst (rreify c) (reify c').
Proof.
  induction 1; intros; unfold rreify, reify; subst.
  { eauto. }
  { eapply rtermR_lambda. intros.
    prove_rtermR.
    match goal with h: context[K] |- _ => erewrite h by eauto; clear h end.
    reflexivity. }
Qed.

(* -------------------------------------------------------------------------- *)

(* We come to the definition of the efficient CPS transformation. *)

(* Every closure in this code is called once and involves operations whose cost
   is constant, except for the renaming operations [interpret] and [_up], whose
   cost is logarithmic. Therefore, this transformation has complexity O(nlog n)
   where [n] is the size of the source term. *)

(* I suspect that going through relocatable terms is in fact a way of first
   translating to a de Bruijn-level-based representation, then going back to
   a de Bruijn-index-based representation. *)

Fixpoint rcps env (t : term) (c : rcontinuation) : rterm :=
  match t with
  | Var x =>
      rapply c (svar x env)
  | Lam t =>
      rapply c (
        slambda env (fun env =>
          lambda (fun k =>
            rcps env t (RO k)
          )
        )
      )
  | App t1 t2 =>
      rcps env t1 (RM (fun v1 =>
        rcps env t2 (RM (fun v2 =>
          app (app v1 v2) (rreify c)
        ))
      ))
  | Let t1 t2 =>
      rcps env t1 (RM (fun v1 =>
        slet env v1 (fun env =>
          rcps env t2 c
        )
      ))
  end.

Definition rinit :=
  RM (fun t => t).

Definition rcpsinit (t : term) : term :=
  let (env, dst) := _id tt in
  rcps env t rinit dst.

(* -------------------------------------------------------------------------- *)

(* A few technical preparations for the theorem that follows. *)

Lemma cps_renaming_up_up:
  forall t c i,
  (cps t.[up (ren (+1))] (liftc 2 c)).[up (up (ren (+i)))] =
  cps t.[up (ren (+i))].[up (ren (+1))] (liftc (i + 2) c).
Proof.
  intros. erewrite cps_renaming.
  { autosubst. }
  { obvious. }
  { rewrite substc_substc. autosubst. }
Qed.

Lemma help_let:
  forall i1 v1 t2 c j,
  (cps t2.[up (ren (+1))] (liftc 2 c)).[up (v1 .: ren (+i1))].[up (ren (+j))] =
  cps t2.[up (ren (+j + i1))] (liftc 1 (liftc (j + i1) c)).
Proof.
  intros.
  rewrite push_substitution_last_up_hoist by tc.
  rewrite cps_renaming_up_up.
  rewrite cps_kubstitution_1.
  rewrite !substc_substc.
  autosubst.
Qed.

(* -------------------------------------------------------------------------- *)

(* Our final result establishes that the efficient implementation [rcps] is
   equivalent to the inefficient-but-simpler definition [cps]. We begin with
   an inductive statement: *)

Theorem rtermR_cps:
  forall t u env dst theta c c',
  renamingR (env, dst) theta ->
  contR dst c c' ->
  t.[theta] = u ->
  rtermR dst (rcps env t c) (cps u c').
Proof.
  size_induction; intros; subst.
  assert (is_value_subst theta). { obvious. } (* this helps the tactic [cps] *)
  destruct t; intros; subst; simpl; cps.
  (* Var *)
  { eapply contR_apply. eauto.
    rewrite cpsv_var_theta by obvious.
    eauto using rtermR_svar. }
  (* Abs *)
  { eapply contR_apply. eauto.
    clear dependent c. clear dependent c'. (* optional *)
    (* At this point, I really do not now how to carry out this proof while
       remaining at a high level, so I just expand definitions away and work
       at a lower level. This works out pretty well. *)
    prove_rtermR.
    destruct env as [ src m ].
    unfold slambda, lambda, sbind, _up.
    cpsv.
    rewrite !subst_lam. do 2 f_equal.
    erewrite cps_renaming by obvious.
    eapply use_rtermR_at_zero.
    eapply IH; obvious.
    { renamingR. }
    { eauto using contR, rtermR_var. }
    { autosubst. }
  }
  (* App *)
  { eapply IH; obvious.
    econstructor; intros i1 ? v1 v'1 ? ?; subst.
    erewrite push_substitution_last by tc.
    erewrite cps_renaming by obvious.
    rewrite lift_up by tc.
    rewrite cps_kubstitution_0.
    eapply IH; [ size | renamingR | | autosubst ].
    econstructor; intros i2 ? v2 v'2 ? ?; subst. asimpl.
    rewrite <- plus_assoc.
    repeat eapply rtermR_app;
    eauto using rtermR_lift, contR_reify.
  }
  (* Let *)
  { eapply IH; obvious.
    econstructor; intros i1 ? v1 v'1 ? ?; subst.
    prove_rtermR.
    unfold slet, sbind. rewrite !subst_let. f_equal.
    { pick rtermR ltac:(fun h => rewrite h by eauto). reflexivity. }
    destruct env as [ src m ].
    rewrite help_let.
    eapply use_rtermR_at_zero.
    eapply IH; eauto using contR_liftc with obvious.
    { renamingR. }
    { autosubst. }
  }
Qed.

(* This corollary is simpler: *)

Lemma rcpsinit_equals_cpsinit:
  forall t,
  rcpsinit t = cpsinit t.
Proof.
  unfold rcpsinit, cpsinit, _id. intros t.
  eapply use_rtermR_at_zero.
  eapply rtermR_cps.
  { renamingR. }
  { econstructor. eauto. }
  { autosubst. }
Qed.
