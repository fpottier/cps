Require Import Coq.Lists.List.
Require Import Coq.Strings.String.
Open Scope string_scope.
Require Import MyTactics.
Require Import LambdaCalculusSyntax.

(* In a raw term, every name occurrence is represented as a string. *)

Inductive raw_term :=
| RawVar (x : string)
| RawLam (x : string) (t : raw_term)
| RawApp (t1 t2 : raw_term)
| RawLet (x : string) (t1 : raw_term) (t2 : raw_term).

(* A poor man's choice function for new names. *)

Definition new_var (env : list string) : string :=
  let n := List.length env in
  match n with
  | 0 => "a"
  | 1 => "b"
  | 2 => "c"
  | 3 => "d"
  | 4 => "e"
  | 5 => "f"
  | _ => "error"
  end.

(* A poor man's conversion of terms to raw terms. *)

Fixpoint export (env : list string) (t : term) : raw_term :=
  match t with
  | Var x =>
      RawVar (nth x env "error")
  | Lam t =>
      let x := new_var env in
      let env := x :: env in
      RawLam x (export env t)
  | App t1 t2 =>
      RawApp (export env t1) (export env t2)
  | Let t1 t2 =>
      let t1 := export env t1 in
      let x := new_var env in
      let env := x :: env in
      RawLet x t1 (export env t2)
  end.

(* A printer in TeX abstract syntax. *)

Fixpoint printl level (t : raw_term) : string :=
  let parenthesize :=
    match level, t with
    | 0, RawApp _ _
    | (0 | 1), RawLam _ _
    | (0 | 1), RawLet _ _ _ =>
        fun (s : string) => "(" ++ s ++ ")"
    | _, _ =>
        fun (s : string) => s
    end
  in
  parenthesize (
    match t with
    | RawVar x =>
        "\Var{" ++ x ++ "}"
    | RawLam x t =>
        "\Lam{" ++ x ++ "." ++ printl 2 t ++ "}"
    | RawApp t1 t2 =>
        "\App{" ++ printl 1 t1 ++ "}{" ++ printl 0 t2 ++ "}"
    | RawLet x t1 t2 =>
        "\Let{" ++ x ++ "=" ++ printl 2 t1 ++ "}{" ++ printl 2 t2 ++ "}"
    end
  ).

Definition print :=
  printl 2.
