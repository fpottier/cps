Require Import Coq.Arith.Compare_dec. (* [lt_dec] *)
Require Import MyTactics.
Require Import Autosubst.Autosubst.
Require Import Autosubst_IsRen.

(* -------------------------------------------------------------------------- *)

(* We now wish to make progress towards an efficient machine representation of
   renamings. The renamings that we have used so far are mathematical
   functions of type [var -> var] or [var -> term]. In an implementation, a
   renaming must be represented as some kind of finite map with good time
   complexity. The time complexity of a lookup should be logarithmic or (even
   better) constant. *)

(* Let us assume an implementation of finite maps (of integers to integers),
   whose correctness we do not establish in Coq. This code will be written
   directly in OCaml. We require an abstract type [map] as well as three
   operations [empty], [add], [find]. *)

(* We will be using these maps in such a way that the domain of a map always
   is an interval of the form [0..src). Furthermore, the operation [add] is
   always used to extend the domain at [src], never to override an existing
   entry. Thus, these maps can also be viewed as stacks (with random read
   access). We do not prove these facts in Coq, but will possibly exploit
   them in our OCaml implementations. *)

Parameter map : Type.

Parameter empty : unit -> map.

Parameter add : nat -> nat -> map -> map.

Parameter find : nat -> map -> nat.

(* The specification of [empty], [add], [find] is standard. We assume that
   there is a representation predicate [mapR], which relates a map with its
   logical model (a relation between keys and values). *)

Parameter mapR : map -> (nat -> nat -> Prop) -> Prop.

(* [empty tt] represents the empty map. We let [empty] be a function of type
   [unit -> map], as opposed to a constant of type [map], as this allows an
   OCaml implementation to have an (initialization) side effect. *)

Parameter empty_spec:
  mapR (empty tt) (fun _ _ => False).

(* [add] destroys any pre-existing edge out of [src], and adds an edge
   from [src] to [dst]. *)

Parameter add_spec:
  forall m M src dst,
  mapR m M ->
  mapR (add src dst m) (fun x y => x = src /\ y = dst \/ x <> src /\ M x y).

(* [find] is presented as a total function (which cannot fail). Its behavior
   is specified only when [x] is in the domain, that is, when there is an
   edge from [x] to some [y]. *)

Parameter find_spec:
  forall m M x y,
  mapR m M ->
  M x y ->
  find x m = y.

(* The following two lemmas are simple consequences of the above spec. *)

Lemma find_add_hit:
  forall m M x y,
  mapR m M ->
  find x (add x y m) = y.
Proof.
  intros. eapply find_spec.
  { eauto using add_spec. }
  { simpl. eauto. }
Qed.

Lemma find_add_miss:
  forall m M x1 x2 y1 y2,
  mapR m M ->
  x1 <> x2 ->
  M x1 y1 ->
  find x1 (add x2 y2 m) = find x1 m. (* which is also [y1] *)
Proof.
  intros. eapply find_spec.
  { eauto using add_spec. }
  { simpl. erewrite find_spec by eauto. eauto. }
Qed.

Ltac find_add :=
  match goal with |- context[find ?x (add ?src _ _)] =>
    destruct (Nat.eq_dec x src);
    [ subst x; erewrite find_add_hit by eauto
    | erewrite find_add_miss by eauto ]
  end.

(* -------------------------------------------------------------------------- *)

(* We now propose a representation of renamings on top of finite maps. *)

(* The renamings that we are interested in map de Bruijn indices to de Bruijn
   indices. The operations on renamings that we must support are [up _], which
   lifts both the source and target name spaces and adds an edge from 0
   towards 0, and [_ >> ren (+1)], which lifts the target name space only and
   adds no new edge. *)

(* It is not obvious at first how to efficiently support these operations,
   which affect both the domain and the codomain of the renaming.

   A key trick is to work internally with de Bruijn levels instead of de
   Bruijn indices, that is, to count up towards the most recent bindings,
   instead of counting up towards the most ancient bindings. If we keep track
   of [src] and [dst], the number of existing names in the source and target
   name spaces, then a de Bruijn index is converted to a de Bruijn level (and
   vice versa) via the operation [x => src - 1 - x] in the source name space
   and via the operation [x => dst - 1 - x] in the target name space.

   There remains to implement a mapping of de Bruijn levels to de Bruijn
   levels. To implement [up _], we must add an edge from [src] to [dst] and
   increment both [src] and [dst]. To implement [_ >> ren (+1)], we must
   increment [dst]. Any implementation of finite maps from the interval
   [0..src) to the interval [0..dst) can be used to store the edges. *)

(* In summary, a renaming is represented as a triple of a source de Bruijn
   level [src], a target de Bruijn level [dst], and a finite map of levels
   to levels, whose domain is the interval [0..src) and whose codomain is
   a subset of the interval [0..dst). *)

(* We write such a triple as [(src, m, dst)]. It can also be viewed as a
   pair [((src, m), dst)]. *)

Definition renaming : Type :=
  (nat * map) * nat.

(* The identity renaming [_id] is represented as a triple of 0, 0, and
   an empty map. *)

Definition _id (_ : unit) : renaming :=
  (0, empty tt, 0).

(* The operation [_up] creates an edge of [src] to [dst] and increments
   both [src] and [dst]. *)

Definition _up (rho : renaming) : renaming :=
  match rho with (src, m, dst) =>
    (1 + src, add src dst m, 1 + dst)
  end.

(* The operation [_fresh i] allocates [i] new names at once in the target
   name space. It is implemented by adding [i] to [dst]. *)

Definition _fresh (rho : renaming) (i : nat) : renaming :=
  match rho with (env, dst) =>
    (env, i + dst)
  end.

(* The operation [interpret] interprets a renaming [(src, m, dst)] as a
   mathematical renaming of type [var -> var]. It is used when we wish to
   apply a renaming to a variable (a source de Bruijn index) to obtain a
   variable (a target de Bruijn index). *)

Definition interpret (rho : renaming) : var -> var :=
  fun x =>
    match rho with (src, m, dst) =>
      (* If [x] is in the interval [0..src), *)
      if lt_dec x src then
        (* Convert [x] to a de Bruijn level, *)
        let x := src - 1 - x in
        (* follow an edge from [x] to [y] in the map [m], *)
        let y := find x m in
        (* and convert [y] back to a de Bruijn index. *)
        let y := dst - 1 - y in
        y
      (* If [x] lies beyond the interval [0..src), then we wish
         to behave as the identity renaming, so to speak. We
         wish to map [src] to [dst], [src + 1] to [dst + 1],
         and so on. This is done as follows. *)
      else
        x + dst - src
    end.

(* -------------------------------------------------------------------------- *)

(* The following lemmas fold the definitions of [_up] and [_fresh]. *)

Lemma fold_fresh:
  forall env dst i,
  (env, i + dst) = _fresh (env, dst) i.
Proof.
  reflexivity.
Qed.

Lemma fold_up:
  forall src m dst,
  (1 + src, add src dst m, 1 + dst) = _up (src, m, dst).
Proof.
  reflexivity.
Qed.

(* -------------------------------------------------------------------------- *)

(* The following basic arithmetic lemmas are used in the proofs that follow. *)

Opaque minus plus.

Lemma rule00: forall x y, 1 + x - 1 - y = x - y.
Proof. intros. omega. Qed.

Lemma rule01: forall x y, y - S x = y - 1 - x.
Proof. intros. omega. Qed.

Lemma rule02: forall x y, y < x -> S (x - 1 - y) = x - y.
Proof. intros. omega. Qed.

Lemma rule03: forall i x y, y < x -> i + x - 1 - y = i + (x - 1 - y).
Proof. intros. omega. Qed.

Lemma arith00: forall x y, x < 1 + y -> x <> y -> x < y.
Proof. intros. omega. Qed.

Lemma arith01: forall x y i, x < y -> x < i + y.
Proof. intros. omega. Qed.

Lemma arith02: forall x y, S x < 1 + y -> x < y.
Proof. intros. omega. Qed.

Lemma arith03: forall x y, x < y -> x <> y.
Proof. intros. omega. Qed.

Lemma arith04: forall x y, x < y -> y - 1 - x < y.
Proof. intros. omega. Qed.

Local Hint Resolve arith00 arith01 arith02 arith03 arith04.

Ltac lt_dec :=
  match goal with |- context[lt_dec ?x ?y] =>
    destruct (lt_dec x y); try solve [ false; omega ]
  end.

(* -------------------------------------------------------------------------- *)

(* Let us now prove that the above machine representation of renamings is
   correct. *)

(* We introduce a representation predicate, [renamingR rho theta], which
   asserts that the machine renaming [rho] represents the mathematical
   renaming [theta]. *)

Section RenamingR.

Context
  {term : Type}
  {Ids_term : Ids term}
  {Rename_term : Rename term}
  {Subst_term : Subst term}
  {SubstLemmas_term : SubstLemmas term}.

Inductive renamingR : renaming -> (var -> term) -> Prop :=
| RenamingR:
    forall src dst m M theta,
    let rho := (src, m, dst) in
    (* 1. The map [m] is well-formed and represents a relation [M]
          between source and target de Bruijn levels. *)
    mapR m M ->
    (* 2. The interval [0..src) is a subset of the domain of [M]. *)
    (forall x, x < src -> M x (find x m)) ->
    (* 3. The image of this interval through [M] is a subset of the
          interval [0..dst). *)
    (forall x, x < src -> find x m < dst) ->
    (* 4. For every source de Bruijn level (possibly beyond [src]),
          the functions [interpret rho] and [theta] agree. *)
    (forall x, ids (interpret rho x) = theta x) ->
    (* *)
    renamingR rho theta.

(* For convenience, this lemma isolates property 4 above. *)

Lemma use_renamingR:
  forall {rho theta x},
  renamingR rho theta ->
  ids (interpret rho x) = theta x.
Proof.
  induction 1; eauto.
Qed.

(* If [renamingR _ theta] holds, then [theta] is a renaming. *)

Lemma is_ren_renamingR:
  forall {rho theta},
  renamingR rho theta ->
  is_ren theta.
Proof.
  intros. exists (interpret rho). f_ext; intros x.
  symmetry. eauto using use_renamingR.
Qed.

(* The identity renaming [_id] represents the identity renaming [ids]. *)

Lemma interpret_identity:
  renamingR (_id tt) ids.
Proof.
  econstructor; intros.
  { eapply empty_spec. }
  { omega. }
  { omega. }
  { simpl. rewrite Nat.sub_0_r, Nat.add_0_r. reflexivity. }
Qed.

(* [_up] correctly implements [up]. *)

Lemma interpret_up:
  forall {rho theta},
  renamingR rho theta ->
  renamingR (_up rho) (up theta).
Proof.
  induction 1 as [ src dst m M theta rho HmapR Hdom Hcodom Hcoincidence ].
  econstructor; intros.
  (* Preservation of [HmapR]. *)
  { eauto using add_spec. }
  (* Preservation of [Hdom]. *)
  { find_add; eauto. }
  (* Preservation of [Hcodom]. *)
  { find_add; eauto. }
  (* Preservation of [Hcoincidence]. *)
  { simpl. rewrite !rule00.
    destruct x; lt_dec.
    (* Case: [x] is zero. *)
    { rewrite Nat.sub_0_r.
      erewrite find_add_hit by eauto.
      rewrite !Nat.sub_diag.
      reflexivity. }
    (* Case: [x] is nonzero and less than [1 + src]. *)
    { asimpl. rewrite <- Hcoincidence. asimpl. lt_dec.
      rewrite rule01.
      rewrite rule02 by eauto.
      erewrite find_add_miss by eauto.
      reflexivity. }
    (* Case: [x] is greater or equal to [1 + src]. *)
    { asimpl. rewrite Nat.sub_succ. rewrite <- Hcoincidence.
      asimpl. lt_dec. compute. f_equal. omega. }
  }
Qed.

(* [_fresh _ i] correctly implements [_ >> ren (+i)]. *)

Lemma interpret_fresh:
  forall {rho theta i},
  renamingR rho theta ->
  renamingR (_fresh rho i) (theta >> ren (+i)).
Proof.
  induction 1 as [ src dst m M theta rho HmapR Hdom Hcodom Hcoincidence ].
  econstructor; intros; eauto.
  (* Preservation of [Hcoincidence]. *)
  simpl. rewrite <- Hcoincidence. asimpl. lt_dec.
  (* Case: [x < src]. *)
  { rewrite rule03 by eauto. reflexivity. }
  (* Case: [x >= src]. *)
  { compute. f_equal. omega. }
Qed.

End RenamingR.

Hint Resolve interpret_identity interpret_up interpret_fresh : renamingR.

Hint Resolve is_ren_renamingR : is_ren obvious.

Hint Rewrite fold_fresh fold_up : fold_renaming.

(* The tactic [fold_renaming] recognizes instances of [_up] and [_fresh]. *)

Ltac fold_renaming :=
  repeat first [ rewrite fold_up | rewrite fold_fresh ].

(* The tactic [renamingR] tries to prove a goal of the form [renamingR _ _]. *)

Ltac renamingR :=
  fold_renaming; eauto with renamingR typeclass_instances.
