Require Import MyTactics.
Require Import FixExtra.
Require Import Sequences.
Require Import LambdaCalculusSyntax.
Require Import LambdaCalculusPrinter.
Require Import LambdaCalculusValues.
Require Import LambdaCalculusReduction.
Require Import CPSDefinition.
Require Import CPSCounterExample.

(* This is Danvy and Nielsen's one-pass, first-order CPS transformation,
   extended with support for the [Let] construct, in the same way as in
   Minamide and Okuma's paper (I hope!).
   http://www.is.titech.ac.jp/~minamide/cps/CPS/LetDanvyNielsen.html *)

(* So far, I have proved nothing about this definition, so it could be wrong
   (e.g., a lift operation could be missing or incorrect). Be careful. *)

(* -------------------------------------------------------------------------- *)

(* The auxiliary function [dncpsv_] defines the translation of values.
   It is parameterized over the main function [dncps_]. *)

Definition dncpsv_
  (v : term)
  (dncps_ : forall t : term, size t < size v -> term -> term)
  : term.
Proof.
  destruct v as [ x | t | t1 t2 | t1 t2 ].
  (* Case: [Var x]. *)
  { refine (Var x). }
  (* Case: [Lam t]. *)
  { refine (Lam (Lam (dncps_ (lift 1 t) _ (Var 0)))).
    abstract size. }
  (* Case: [App]. (Dummy.) *)
  { refine (Lam (Var 0)). }
  (* Case: [Let]. (Dummy.) *)
  { refine (Lam (Var 0)). }
Defined.

(* -------------------------------------------------------------------------- *)

(* [dncps t k] is the translation of the term [t] with continuation [k]. *)

Hint Extern 1 (size ?t < ?n) =>
  rewrite size_renaming in * by obvious
  : size obvious.

Notation App2 t0 t1 t2 :=
  (App (App t0 t1) t2).

Definition dncps : term -> term -> term.
Proof.
  refine (Fix smaller_wf_transparent (fun _ => term -> term) _).
  intros t dncps_ k.
  (* In the following, whenever we wish to call [dncpsv_], we pass
     the argument [fun v _ k => dncps_ v _ k], which is just [dncps_],
     eta-expanded so that the size constraint in the type of [dncps_]
     can be replaced with a logically equivalent constraint. *)
  destruct t as [ x | t | t1 t2 | t1 t2 ].
  (* Case: [Var x]. *)
  { refine (App k (dncpsv_ (Var x) dncps_)). }
  (* Case: [Lam t]. *)
  { refine (App k (dncpsv_ (Lam t) dncps_)). }
  (* Case: [App t1 t2]. *)
  { refine (if_value t1 _ _).
    (* Subcase: [t1] is a value. *)
    { refine (if_value t2 _ _).
      (* Subsubcase: [t2] is a value. *)
      { refine (
          App2
            (dncpsv_ t1 (fun v _ k => dncps_ v _ k))
            (dncpsv_ t2 (fun v _ k => dncps_ v _ k))
            k
        );
        abstract size. }
      (* Subsubcase: [t2] is not a value. *)
      { refine (
          dncps_ t2 _ (Lam (
            App2
              (dncpsv_ (lift 1 t1) (fun v _ k => dncps_ v _ k))
              (Var 0)
              (lift 1 k)
          ))
        ); abstract size.
      }
    }
    (* Subcase: [t1] is not a value. *)
    { refine (if_value t2 _ _).
      (* Subsubcase: [t2] is a value. *)
      { refine (
          dncps_ t1 _ (Lam (
            App2
              (Var 0)
              (dncpsv_ (lift 1 t2) (fun v _ k => dncps_ v _ k))
              (lift 1 k)
          ))
        ); abstract size.
      }
      (* Subsubcase: [t2] is not a value. *)
      { refine (
          dncps_ t1 _ (Lam (
            dncps_ (lift 1 t2) _ (Lam (
              App2 (Var 1) (Var 0) (lift 2 k)
            ))
          ))
        );
        abstract size.
      }
    }
  }
  (* Case: [Let x = t1 in t2]. *)
  { refine (if_value t1 _ _).
    (* Subcase: [t1] is a value. *)
    { refine (
        Let (dncpsv_ t1 (fun v _ k => dncps_ v _ k))
        (dncps_ t2 _ (lift 1 k))
      );
      abstract size. }
    (* Subcase: [t1] is not a value. *)
    (* We seem to be generating an essentially useless [Let] construct,
       of the form [let variable = variable in _]. In this situation,
       Danvy and Filinski's transformation can produce a more interesting
       [let variable = value in _]. *)
    { refine (
        dncps_ t1 _ (Lam (
          Let (Var 0)
          (dncps_ t2.[up (ren (+1))] _ (lift 2 k))
        ))
      );
      abstract size. }
  }
Defined.

(* -------------------------------------------------------------------------- *)

(* [dncpsv v] is the translation of the value [v]. *)

Definition dncpsv (v : term) : term :=
  dncpsv_ v (fun t _ k => dncps t k).

(* The initial continuation is used when invoking [dncps] at the top level. *)

Definition dninit :=
  Lam (Var 0).

Definition dncpsinit t :=
  dncps t dninit.

(* -------------------------------------------------------------------------- *)

Transparent cps cpsv.

(* On terms without [Let], Danvy-Filinski and Danvy-Nielsen coincide. *)

Goal
  let t := Lam (Var 0) in
  let k := Var 0 in
  dncps t k = cps t (O k).
Proof.
  compute. reflexivity.
Qed.

Goal
  let t := App (Lam (Var 0)) (Var 0) in
  let k := Var 1 in
  dncps t k = cps t (O k).
Proof.
  compute. reflexivity.
Qed.

(* -------------------------------------------------------------------------- *)

(* The terms [t1] and [t2] used here are defined in [CPSCounterExample]. *)

(* The term [t1] is translated in the same way by Danvy-Filinski and by
   Danvy-Nielsen under their respective initial continuations [init] and
   [dninit], but the term [t2] isn't. *)

Goal
  cpsinit t1 = dncpsinit t1.
Proof.
  compute. reflexivity.
Qed.

Goal
  cpsinit t2 <> dncpsinit t2.
Proof.
  compute. congruence.
Qed.

(* The reduction step [t1 -> t2] is NOT a counter-example to Danvy and
   Nielsen's simulation diagram for Danvy and Nielsen's transformation.
   Which explains why Minamide and Okuma are able to prove that this
   diagram holds. *)

Goal
  star cbv (dncpsinit t1) (dncpsinit t2).
Proof.
  Opaque cbv_mask. compute. Transparent cbv_mask.
  step. asimpl.
  step. asimpl.
  finished. reflexivity.
Qed.

(* -------------------------------------------------------------------------- *)

(* This example shows that Danvy and Nielsen's translation, extended with [Let]
   by Minamide and Okuma, produces one administrative redex which Danvy and
   Filinski's translation does not produce. *)

(* Note that this example involves only one [Let] construct, whose left-hand
   side is a value. This construct is placed in the context [App _ (Var 0)],
   so, under Danvy-Filinski, it is transformed under an [M] continuation,
   whereas under Danvy-Nielsen, it is effectively transformed under an [O]
   continuation. *)

Definition t'2 := App t2 (Var 0).

Goal
  dncpsinit t'2 <> cps t'2 (O dninit).
Proof.
  intros. compute. congruence.
Qed.

(* The following can be used to obtain the translations of [t'2] in TeX form:
Require Import Coq.Lists.List.
Require Import Coq.Strings.String.
Open Scope string_scope.
Eval compute in print (export ("y" :: nil) (dncpsinit t'2)).
Eval compute in print (export ("y" :: nil) (cps t'2 (O dninit))). *)

(* Other examples. *)

Goal
  let t := Let (Let (Var 0) (Var 0)) (Var 0) in
  dncpsinit t <> cps t (O dninit).
Proof.
  intros. compute. congruence.
Qed.

Goal
  let t := Let (Let (Let (Var 0) (Var 0)) (Var 0)) (Var 0) in
  dncpsinit t <> cps t (O dninit).
Proof.
  intros. compute. congruence.
Qed.
